@extends('Layouts.dashboardLayout')
@section('content')

    <div class="slim-mainpanel">
        <div class="container">
            <div class="slim-pageheader">
                <ol class="breadcrumb slim-breadcrumb">
                    <li class="breadcrumb-item"><a href="{{route('dashboard')}}">Dashboard</a></li>
                    <li class="breadcrumb-item"><a href="{{route('allProjects')}}">All Projects</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Task</li>
                </ol>
                <h6 class="slim-pagetitle">@if(count($tasks) > 0) Task for project {{$tasks[0]->project->name}}@endif</h6>
            </div><!-- slim-pageheader -->

            <div class="section-wrapper">
                <label class="section-title">@if(count($tasks) > 0)All Tasks under {{$tasks[0]->project->name}}@endif</label>
                {{--<p class="mg-b-20 mg-sm-b-40">Projects Have tasks contained in them.  The tasks are tracked </p>--}}
                <div class="timeline-group">
                    <div class="timeline-item timeline-day">
                        <div class="timeline-time">&nbsp;</div>
                        <div class="timeline-body">
                            <p class="timeline-date">Project Tasks</p>
                        </div><!-- timeline-body -->
                    </div><!-- timeline-item -->
                    @if(count($tasks) > 0)
                    @foreach($tasks as $task)
                        <div class="timeline-item">
                            <div class="timeline-time">{{date('jS F Y h:i a', strtotime($task->expected_start_date))}}</div>
                            <div class="timeline-body">
                                <p class="timeline-title"><a href="#">{{$task->task_name}}</a></p>
                                <p class="timeline-author">Created by : <a href="#"> {{$task->user->firstname}}</a> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Completion status: {{number_format((($task->taskCount->process_count - ($task->skipped != null ? count( $task->skipped) : 0))/($task->skipped != null ?  8 - count( $task->skipped) : 8))*100, '2')}} %</b></p>
                                <p class="timeline-text">{{$task->description}} </p>
                                <p class="tx-12 mg-b-0"><a href="#" data-toggle="modal" data-target="#details{{$task->id}}">View Stages</a> &nbsp;&nbsp; <a class="popEdit" href="javascript:void(0)" data-id="{{$task->id}}" data-name="{{$task->task_name}}" data-amt="{{$task->amount_due}}" data-vendor="{{$task->vendor}}" data-supervisor="{{$task->supervisor}}" data-project="{{$task->project_type}}" data-edate="{{$task->expected_finish_date}}" data-desc="{{$task->description}}" data-sdate="{{$task->expected_start_date}}" data-cname="{{$task->client_name}}" data-cemail="{{$task->client_email}}" data-toggle="modal" data-target="#editTask">Edit</a> &nbsp;<a href="">Delete</a> &nbsp;@if($task->taskCount->process_count == 8 && $task->actual_completion_date == null) This task is seen to be complete <button class="btn btn-sm btn-info" data-toggle="modal" data-tid="{{$task->id}}" data-target="#verifyCompletion" id="popVerify">Verify Completion</button> @endif  @if($task->updated_by != null) Last updated by : {{$task->updatingTask->firstname}}@endif</p>
                            </div><!-- timeline-body -->
                        </div><!-- timeline-item -->
                    @endforeach
                    @else
                        <div><p>No task(s) yet</p></div>
                    @endif
                </div>
            </div>

        </div>

        {{--Modal force checking --}}
        @if(count($tasks) > 0)
        @foreach($tasks as $task)
        <div id="details{{$task->id}}" class="modal fade" style="display: none;" aria-hidden="true">
            <div class="modal-dialog modal-dialog-vertical-center" role="document">
                <div class="modal-content bd-0 tx-14">
                    <div class="modal-header">
                        <h6 class="tx-14 mg-b-0 tx-uppercase tx-inverse tx-bold"></h6>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <div class="modal-body pd-25">
                        <h5 class="lh-3 mg-b-20"><a href="#" class="tx-inverse hover-primary">Below shows the stage of this task</a></h5>
                        <p class="mg-b-5">
                            <div class="row">
                                <ul class="list-group">
                                    <li class="list-group-item {{($task->quotation_status == true ? '' : 'undone')}}" data-taskid="{{$task->id}}" data-name="quotation_sent">
                        <p class="mg-b-0"><strong class="tx-inverse tx-medium">Quotation Prepared and Sent</strong> <span class="text-muted"> {!!($task->quotation_status == true ? '<i class="fa fa-check-circle correct"></i>' : '') !!} </span></p>
                        </li>
                        <li class="list-group-item {{($task->client_approval == true ? '' : 'undone')}}" data-taskid="{{$task->id}}" data-name="quotation_approved">
                            <p class="mg-b-0"><strong class="tx-inverse tx-medium">Quotation Approved</strong> <span class="text-muted">{!!($task->client_approval == true ? '<i class="fa fa-check-circle correct"></i>' : '') !!}</span></p>
                        </li>
                        <li class="list-group-item {{($task->head_office_informed == true ? '' : 'undone')}}" data-taskid="{{$task->id}}" data-name="head_office_informed">
                            <p class="mg-b-0"><strong class="tx-inverse tx-medium">Inform Head Office</strong> <span class="text-muted">{!!($task->head_office_informed == true ? '<i class="fa fa-check-circle correct"></i>' : '')!!}</span></p>
                        </li>
                        <li class="list-group-item {{($task->head_office_approval == true ? '' : 'undone')}}" data-taskid="{{$task->id}}" data-name="head_office_approval">
                            <p class="mg-b-0"><strong class="tx-inverse tx-medium">Head Office Approves</strong> <span class="text-muted">{!! ($task->head_office_approval == true ? '<i class="fa fa-check-circle correct"></i>' : '')!!}</span></p>
                        </li>
                        <li class="list-group-item {{($task->execution_status == true ? '' : 'undone')}}" data-taskid="{{$task->id}}" data-name="execution_status">
                            <p class="mg-b-0"><strong class="tx-inverse tx-medium">Execution</strong> <span class="text-muted">{!! ($task->execution_status == true ? '<i class="fa fa-check-circle correct"></i>' : '')!!}</span></p>
                        </li>
                        <li class="list-group-item {{($task->jcc_status == true ? '' : 'undone')}}" data-taskid="{{$task->id}}" data-name="jcc_status">
                            <p class="mg-b-0"><strong class="tx-inverse tx-medium">JCC Granted</strong> <span class="text-muted">{!!  ($task->jcc_status == true ? '<i class="fa fa-check-circle correct"></i>' : '')!!}</span></p>
                        </li>
                        <li class="list-group-item {{($task->invoice_status == true ? '' : 'undone')}}" data-taskid="{{$task->id}}" data-name="invoice_status">
                            <p class="mg-b-0"><strong class="tx-inverse tx-medium">Invoice Generated</strong> <span class="text-muted">{!! ($task->invoice_status == true ? '<i class="fa fa-check-circle correct"></i> <a href="/generateInvoice/'.$task->id.'">Generate</a>' : '')!!}</span></p>
                        </li>
                        <li class="list-group-item {{($task->payment_status == true ? '' : 'undone')}}" data-taskid="{{$task->id}}" data-name="payment_status">
                            <p class="mg-b-0"><strong class="tx-inverse tx-medium">Payment Made</strong> <span class="text-muted">{!! ($task->payment_status == true ? '<i class="fa fa-check-circle correct"></i>' : '')!!}</span></p>
                        </li>
                        </ul>
                    </div>
                        </p>
                    @if($task->project->completion_status == 0)<div><button class="btn btn-sm btn-info float-md-right backwardStep">Move a step backwards</button></div>@endif
                    @if($task->project->completion_status == 0)<div><button class="btn btn-sm btn-warning float-md-right skip">Skip next step</button></div>@endif
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-success unlock">Unlock next stage</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    </div>
                </div>
        </div><!-- modal-dialog -->
            </div>
        @endforeach
        @endif



    {{--Edit task--}}
    <div id="editTask" class="modal fade" aria-hidden="true">
        <div class="modal-dialog modal-dialog-vertical-center" role="document">
            <div class="modal-content bd-0 tx-14">
                <div class="modal-header">
                    <h6 class="tx-14 mg-b-0 tx-uppercase tx-inverse tx-bold">Edit task</h6>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body pd-25">
                    <input type="hidden" id="theid">
                    <div class="form-group">
                        <label>Name</label>
                        <input type="text" id="tskname" class="form-control">
                    </div>
                    <div class="form-group">
                        <label>Task Estimete Budget</label>
                        <input type="text" id="tskamt" class="form-control">
                    </div>
                    <div class="form-group">
                        <label>Task Description</label>
                        <textarea class="form-control" id="descrptn"></textarea>
                    </div>
                    <div class="form-group">
                        <label>Expected Start Date</label>
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <div class="input-group-text">
                                    <i class="fa fa-calendar tx-16 lh-0 op-6"></i>
                                </div>
                            </div>
                            <input type="text" name="esdate" id="esdate" class="form-control fc-datepicker" data-provide='datepicker' data-date-container='#addTask' placeholder="YYYY-MM-DD">
                        </div>
                    </div>
                    <div class="form-group">
                        <label>Expected Start Date</label>
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <div class="input-group-text">
                                    <i class="fa fa-calendar tx-16 lh-0 op-6"></i>
                                </div>
                            </div>
                            <input type="text" name="efdate" id="efdate" class="form-control fc-datepicker" data-provide='datepicker' data-date-container='#addTask' placeholder="YYYY-MM-DD">
                        </div>
                    </div>
                    <div class="form-group">
                        <label>Task Type</label>
                        <select class="form-control" id="ptype" name="ptype">
                            <option disabled selected>Select Type</option>
                            <option>Routine</option>
                            <option>Ad-hoc</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Client's Name</label>
                        <select class="form-control" id="cname" name="cname">
                            {{--<option disabled>All</option>--}}
                            @foreach($clients as $client)
                                <option data-id="{{$client->id}}">{{$client->name}}</option>
                            @endforeach
                        </select>
                        {{--<input type="text" name="cname" id="cname" class="form-control">--}}
                    </div>
                    <div class="form-group">
                        <label>Client's Email Address</label>
                        <input type="text" name="cemail" id="cemail" class="form-control">
                    </div>
                    <div id="dep" class="doNotShow">
                        <div class="form-group">
                            <label>Supervisor</label>
                            <select class="form-control" id="supervisor" name="supervisor">
                                {{--<option disabled>All</option>--}}
                                @foreach($supervisors as $supervisor)
                                    <option data-id="{{$supervisor->id}}">{{$supervisor->firstname}} {{$supervisor->lastname}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Vendor</label>
                            <select class="form-control" id="vendor" name="vendor">
                                {{--<option disabled>All</option>--}}
                                @foreach($vendors as $vendor)
                                    <option data-id="{{$vendor->id}}">{{$vendor->firstname}} {{$vendor->lastname}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-success" id="editTsk">Edit</button>
                </div>
            </div>
        </div>
    </div>

    {{--Modal verifying completion --}}
    <div id="verifyCompletion" class="modal fade" aria-hidden="true">
        <div class="modal-dialog modal-dialog-vertical-center" role="document">
            <div class="modal-content bd-0 tx-14">
                <div class="modal-header">
                    <h6 class="tx-14 mg-b-0 tx-uppercase tx-inverse tx-bold"></h6>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body pd-25">
                    <div class="form-group">
                        <label>Please provide the actual completion date</label>
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <div class="input-group-text">
                                    <i class="fa fa-calendar tx-16 lh-0 op-6"></i>
                                </div>
                            </div>
                            <input type="text" name="cdate" id="cdate" class="form-control fc-datepicker" data-provide='datepicker' data-date-container='#addTask' placeholder="YYYY-MM-DD">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-success" id="updateCdate">Verify Completion</button>
                </div>
            </div>
        </div>
    </div>
    @include('includes.alerts')
@endsection
@section('script')
    <script>
        $.ajaxSetup({
            headers:{'X-CSRF-TOKEN': $('meta[name=csrf-token]').attr('content')}
        });

        $('#tskamt').bind('keyup input paste', function(){
            this.value = this.value.replace(/[^0-9,.]/g, '');
        });

        $('#cdate').datepicker({
            showOtherMonths: true,
            selectOtherMonths: true,
            dateFormat: 'yy-mm-dd'
        });

        $('#efdate').datepicker({
            showOtherMonths: true,
            selectOtherMonths: true,
            dateFormat: 'yy-mm-dd'
        });

        $('#esdate').datepicker({
            showOtherMonths: true,
            selectOtherMonths: true,
            dateFormat: 'yy-mm-dd'
        });

        let tid = "";
        $('#popVerify').on('click', function () {
            tid = $(this).data('tid');
        });

        $('#updateCdate').on('click', function () {
            if($('#cdate').val() == "" || $('#cdate').val() == null){
                $('#errors').modal('toggle');
                let msg = "<p>Ooops!</p><p>Please select a valid date.</p>";
                $('#message').html(msg);
            }else{
                $.post("{{route('updateCompletionDate')}}", {id:tid, date: $('#cdate').val()}, function (data) {
                    console.log(data);
                    if(data.task){
                        $('#success').modal('toggle');
                        let msg = "<p>You successfully marked this task completed.</p>";
                        $('#successmsg').html(msg);
                        $('#verifyCompletion').modal('toggle');
                        if($('#success').modal('hide')){
                            location.reload();
                        }
                    }else{
                        $('#errors').modal('toggle');
                        let msg = "<p>Ooops!</p><p>Sth went wrong. Try again!</p>";
                        $('#message').html(msg);
                    }
                });
            }
        });

        $('select[name=cname]').on('change', function(){
//            let r = $(this).find('option:selected').val();
            let r = $(this).find('option:selected').data('id');
            if(r != null && r != "All"){
                $.post('/findemail', {id:r}, function (data) {
                    $('input[name=cemail]').val(data);
                });
            }else{
                $('input[name=cemail]').val('');
            }

        });

        $('select[name=ptype]').on('change', function(){
//            let r = $(this).find('option:selected').val();
            let r = $(this).find('option:selected').text();
            if(r == "Ad-hoc" ){
                $('#dep').removeClass('doNotShow')
            }else{
                $('#dep').addClass('doNotShow')
            }

        });

        $(document).on('click', '.unlock', function () {
            let a = [];
            let parnt = $(this);
            parnt.closest('.modal-content').find('.undone').each(function () {
                a.push([$(this).data('taskid'), $(this).data('name')])
            });
            //console.log(a[0]);
            let send = {};
            if(a.length > 0){
                send[0] = a[0][0];
                send[1] = a[0][1];
                console.log(send);
                parnt.attr('disabled', true);
                parnt.text('Please wait...');
                $.post("{{route('updateTaskStage')}}", send, function (resp) {
                    console.log(resp);
                    parnt.attr('disabled', false);
                    parnt.text('Unlock next stage');
                    if(resp.task){
                        $('#success').modal('toggle');
                        let msg = "<p>You successfully updated the task named "+ resp.task.task_name+"</p>";
                        $('#successmsg').html(msg);
                        parnt.closest('.modal-content').find('.list-group-item').each(function () {
                            let ths = $(this);
                            if(ths.data('name') == a[0][1]){
                                ths.removeClass('undone');
                                ths.find('.text-muted').append('<i class="fa fa-check-circle correct"></i>');
                                if(ths.data('name') == "invoice_status"){
                                    ths.find('.text-muted').append(' <a href="/generateInvoice/'+resp.task.id+'">Generate</a>');
                                }
                            }
                        });
                    }else{
                        $('#errors').modal('toggle');
                        let msg = "<p>Ooops!</p><p>Something Went wrong. Please try again</p>";
                        $('#message').html(msg);
                    }
                });
            }else{
                $('#errors').modal('toggle');
                let msg = "<p>Sorry!</p><p>You cannot proceed beyond this level.</p>";
                $('#message').html(msg);
            }
        });

        $('.backwardStep').on('click', function () {
            let a = [];
            let prnt = $(this);
            $(this).closest('.modal-body').find('.fa-check-circle').each(function () {
                let act = $(this).parent().parent().parent();
//                if(act.data('name') != "quotation_sent" && act.data('name') != "quotation_approved"){
                    a.push([act.data('taskid'), act.data('name')]);
//                }
            });
            //return console.log(a[a.length -1]);
            let send = {};
            if(a.length > 0){
                send[0] = a[a.length -1][0];
                send[1] = a[a.length -1][1];
                console.log(send);
                prnt.attr('disabled', true);
                prnt.text('Moving...');
                $.post("{{route('stepTaskBackwards')}}", send, function (resp) {
                    console.log(resp);
                    prnt.attr('disabled', false);
                    prnt.text('Move a step backwards');
                    if(resp.task){
                        $('#success').modal('toggle');
                        let msg = "<p>You successfully stepped the task named "+ resp.task.task_name+" backwards.</p>";
                        $('#successmsg').html(msg);
                        prnt.closest('.modal-body').find('.list-group-item').each(function () {
                            let ths = $(this);
                            if(ths.data('name') == a[a.length -1][1]){
                                ths.addClass('undone');
                                ths.find('.text-muted').empty();
                            }
                        });
                    }else{
                        $('#errors').modal('toggle');
                        let msg = "<p>Ooops!</p><p>Something Went wrong. Please try again</p>";
                        $('#message').html(msg);
                    }
                });
            }else{
                $('#errors').modal('toggle');
                let msg = "<p>Sorry!</p><p>You cannot proceed beyond this level.</p>";
                $('#message').html(msg);
            }

        });

        $('.skip').on('click', function (){
            let a = [];
            let act;
            let sk = $(this)
            $(this).closest('.modal-body').find('.fa-check-circle').each(function () {
                act = $(this).parent().parent().parent();
                a.push([act.data('taskid'), act.data('name')]);
            });
            let size = a.length;
            if(size == 0){
                let y = $('.list-group-item.undone')
                let use = y.first();
                a.push([use.data('taskid'), 'nothing']);
                act = use;
            }

            console.log(a);

            if(size != 0){
                if(act.next().next().hasClass('undone')){
                    sk.attr('disabled', true);
                    sk.text('Skipping...');
                    $.post('/skipNextStep', {task_id:a[a.length-1][0], current_step:a[a.length-1][1]}, function (resp) {
                        console.log(resp);
                        sk.attr('disabled', false);
                        sk.text('Skip next step');
                        if(resp.task){
                            $('#success').modal('toggle');
                            let msg = "<p>You successfully skipped the task named "+ resp.task.task_name+" by one step.</p>";
                            $('#successmsg').html(msg);
                            sk.closest('.modal-body').find('.list-group-item').each(function () {
                                let ths = $(this);
                                if(ths.data('name') == a[a.length -1][1]){
                                    ths.next().next().removeClass('undone');
                                    ths.next().next().find('.text-muted').append('<i class="fa fa-check-circle correct"></i>');
                                }
                            });
                        }else{
                            $('#errors').modal('toggle');
                            let msg = "<p>Ooops!</p><p>Something Went wrong. Please try again</p>";
                            $('#message').html(msg);
                        }
                    });
                }else{
                    return alert('The step is already checked')
                }
            }else{
                    sk.attr('disabled', true);
                    sk.text('Skipping...');
                    $.post('/skipNextStep', {task_id:a[a.length-1][0], current_step:a[a.length-1][1]}, function (resp) {
                        console.log(resp);
                        sk.attr('disabled', false);
                        sk.text('Skip next step');
                        if(resp.task){
                            $('#success').modal('toggle');
                            let msg = "<p>You successfully skipped the task named "+ resp.task.task_name+" by one step.</p>";
                            $('#successmsg').html(msg);
                            sk.closest('.modal-body').find('.list-group-item').each(function () {
                                let ths = $(this);
                                if(ths.data('name') == 'quotation_approved'){
                                    ths.removeClass('undone');
                                    ths.find('.text-muted').append('<i class="fa fa-check-circle correct"></i>');
                                }
                            });
                        }else{
                            $('#errors').modal('toggle');
                            let msg = "<p>Ooops!</p><p>Something Went wrong. Please try again</p>";
                            $('#message').html(msg);
                        }
                    });

            }
        });

        $(document).on('click', '.popEdit', function () {
            let name = $(this).data('name');
            let id = $(this).data('id');
            let amt = $(this).data('amt');
            let edate = $(this).data('edate');
            let sdate = $(this).data('sdate');
            let descrptn = $(this).data('desc');
            let cname = $(this).data('cname');
            let projecttype = $(this).data('project');
            let vendor = $(this).data('vendor');
            let supervisor = $(this).data('supervisor');
            let cemail = $(this).data('cemail');
            $('select[name=cname]').find('option').each(function () {
                if($(this).text() == cname){
                    $(this).prop('selected', true);
                }else{
                    $(this).prop('disabled', true);
                }
            });
            $('select[name=ptype]').find('option').each(function () {
                if($(this).text() == projecttype){
                    $(this).prop('selected', true);
                }
            });

            $('#tskamt').val(amt);
            $('#tskname').val(name);
            $('#esdate').val(sdate);
            $('#efdate').val(edate);
            $('#theid').val(id);
            $('#descrptn').val(descrptn);
            $('#cname').val(cname);
            $('#cemail').val(cemail);
            if(projecttype == "Ad-hoc"){
                $('#dep').removeClass('doNotShow')
                $('select[name=supervisor]').find('option').each(function () {
                    if($(this).text() == supervisor){
                        $(this).prop('selected', true);
                    }
                });
                $('select[name=vendor]').find('option').each(function () {
                    if($(this).text() == vendor){
                        $(this).prop('selected', true);
                    }
                });
            }else{
                $('#dep').addClass('doNotShow');
            }
        });

        $('#editTsk').on('click', function () {
            let amt = $('#tskamt').val();
            let name = $('#tskname').val();
            let sdate = $('#esdate').val();
            let edate = $('#efdate').val();
            let id = $('#theid').val();
            let descrptn = $('#descrptn').val();
            let cname = $('#cname').find('option:selected').text();
            let projecttype = $('#ptype').find('option:selected').text();
            let vendor = $('#vendor').find('option:selected').text();
            let supervisor = $('#supervisor').find('option:selected').text();
            let cemail= $('#cemail').val();
            if(projecttype == "Routine"){
                vendor = null;
                supervisor =null
            }
            $(this).attr('disabled', true);
            $.post('{{route("editTask")}}', {id:id, amt:amt, name:name, description:descrptn, projecttype:projecttype, vendor:vendor, supervisor:supervisor, sdate:sdate, edate:edate, cname:cname, cemail:cemail}, function (data) {
                console.log(data);
                $('#editTsk').attr('disabled', false);
                if(data.success){
                    $('#success').modal('toggle');
                    let msg = "<p>You successfully updated the task.</p>";
                    $('#successmsg').html(msg);
                    if($('#success').modal('hide')){
                        location.reload();
                    }
                }else{
                    $('#errors').modal('toggle');
                    let msg = "<p>There was an errors creating the project. Please try again.</p>";
                    $('#message').html(msg);
                }
            });
        });
    </script>
@endsection