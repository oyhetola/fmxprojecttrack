@extends('Layouts.pageResponseLayout')
@section('content')
    <h1 class="error-title">updated!</h1>
    <h5 class="tx-sm-24 tx-normal">Next stage Activated.</h5>
    <p class="mg-b-50">You have successfully unlock the next stage of the task {{$task->task_name}}. You can login to view details.</p>
    <p class="mg-b-50">@if(Auth::check()) <a href="{{route('viewLogin')}}" class="btn btn-error">Login</a> @else <a href="{{route('dashboard')}}" class="btn btn-error">Login</a> @endif</p>
    <p class="error-footer">© Copyright {{date('Y')}}. All Rights Reserved. FMX ProjectTracker.</p>
@endsection