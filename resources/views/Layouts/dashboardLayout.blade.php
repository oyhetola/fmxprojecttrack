<!DOCTYPE html>
<html lang="en">

<!-- Mirrored from themepixels.me/slim1.1/template/page-signin2.html by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 02 Jun 2019 13:04:28 GMT -->
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Twitter -->
    <meta name="twitter:site" content="@themepixels">
    <meta name="twitter:creator" content="@themepixels">
    <meta name="twitter:card" content="summary_large_image">
    <meta name="twitter:title" content="FMX Projects">
    <meta name="twitter:description" content="XL Outsourcing Limited | Talent Hub">
    <meta name="twitter:image" content="">
    <meta name="csrf-token" content="{{csrf_token()}}">

    <!-- Facebook -->
    <meta property="og:url" content="">
    <meta property="og:title" content="FMX">
    <meta property="og:description" content="FMX.">

    {{--<meta property="og:image" content="../../slim/img/slim-social.html">
    <meta property="og:image:secure_url" content="../../slim/img/slim-social.html">--}}
    <meta property="og:image:type" content="image/png">
    <meta property="og:image:width" content="1200">
    <meta property="og:image:height" content="600">

    <!-- Meta -->
    <meta name="description" content="Track project, Track">
    <meta name="author" content="ThemePixels">

    <title>FMX | Project Tracker</title>

    <!-- Vendor css -->
    <link href="{{asset('lib/fontawesome/css/fontawesome.css')}}" rel="stylesheet">
    <link href="{{asset('lib/fontawesome/css/all.css')}}" rel="stylesheet">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/smoothness/jquery-ui.css">
    <link href="{{asset('lib/jqvmap/css/jqvmap.min.css')}}" rel="stylesheet">
    <link href="{{asset('lib/jquery.steps/css/jquery.steps.css')}}" rel="stylesheet">
    <link href="{{asset('lib/datatables/css/jquery.dataTables.css')}}" rel="stylesheet">
    <link href="{{asset('lib/select2/css/select2.min.css')}}" rel="stylesheet">

    <!-- Slim CSS -->
    <link rel="stylesheet" href="{{asset('css/slim.css')}}">
    <link rel="stylesheet" href="{{asset('css/app.css')}}">

    <style>
        .doNotShow{
            display: none;
        }

        .error{
            color: red;
        }

        .correct{
            color: #00d600;
        }

        .undone{
            background-color: #cccccc;
        }

        .list-group-item:hover{
            cursor: pointer;
        }

        /*#addTask .ui-datepicker .ui-widget .ui-widget-content .ui-helper-clearfix .ui-corner-all{
            z-index: 1500 !important;
        }*/

    </style>
</head>
<body class="dashboard-4">
<div class="slim-header">
    <div class="container">
        <div class="slim-header-left">
            <h2 class="slim-logo"><a href="">FMX Projects<span></span></a></h2>

            {{--<div class="search-box">
                <input type="text" class="form-control" placeholder="Search">
                <button class="btn btn-primary"><i class="fa fa-search"></i></button>
            </div><!-- search-box -->--}}
        </div><!-- slim-header-left -->
        <div class="slim-header-right">
            {{--<div class="dropdown dropdown-a">
                <a href="#" class="header-notification" data-toggle="dropdown">
                    <i class="icon ion-ios-bolt-outline"></i>
                </a>
                <div class="dropdown-menu">
                    <div class="dropdown-menu-header">
                        <h6 class="dropdown-menu-title">Activity Logs</h6>
                        <div>
                            <a href="#">Filter List</a>
                            <a href="#">Settings</a>
                        </div>
                    </div><!-- dropdown-menu-header -->
                    <div class="dropdown-activity-list">
                        <div class="activity-label">Today, December 13, 2017</div>
                        <div class="activity-item">
                            <div class="row no-gutters">
                                <div class="col-2 tx-right">10:15am</div>
                                <div class="col-2 tx-center"><span class="square-10 bg-success"></span></div>
                                <div class="col-8">cloud storage</div>
                            </div><!-- row -->
                        </div><!-- activity-item -->
                        --}}{{--<div class="activity-label">Yesterday, December 12, 2017</div>--}}{{--

                    </div><!-- dropdown-activity-list -->
                    <div class="dropdown-list-footer">
                        <a href="page-activity.html"><i class="fa fa-angle-down"></i> Show All Activities</a>
                    </div>
                </div><!-- dropdown-menu-right -->
            </div>--}}<!-- dropdown -->
            {{--<div class="dropdown dropdown-b">
                <a href="#" class="header-notification" data-toggle="dropdown">
                    <i class="icon ion-ios-bell-outline"></i>
                    <span class="indicator"></span>
                </a>
                <div class="dropdown-menu">
                    <div class="dropdown-menu-header">
                        <h6 class="dropdown-menu-title">Notifications</h6>
                        <div>
                            <a href="#">Mark All as Read</a>
                            <a href="#">Settings</a>
                        </div>
                    </div><!-- dropdown-menu-header -->
                    <div class="dropdown-list">
                        <!-- loop starts here -->
                        <a href="#" class="dropdown-link">
                            <div class="media">
                                <img src="../img/img8.jpg" alt="">
                                <div class="media-body">
                                    <p><strong>Suzzeth Bungaos</strong> tagged you and 18 others in a post.</p>
                                    <span>October 03, 2017 8:45am</span>
                                </div>
                            </div><!-- media -->
                        </a>
                        <!-- loop ends here -->
                        <div class="dropdown-list-footer">
                            <a href="page-notifications.html"><i class="fa fa-angle-down"></i> Show All Notifications</a>
                        </div>
                    </div><!-- dropdown-list -->
                </div><!-- dropdown-menu-right -->
            </div>--}}<!-- dropdown -->
            <div class="dropdown dropdown-c">
                <a href="#" class="logged-user" data-toggle="dropdown">
                    <img src="{{asset('img/user.png')}}" alt="">
                    <span>@if(Auth::check()){{Auth::user()->firstname}}@endif</span>
                    <i class="fa fa-angle-down"></i>
                </a>
                @if(Auth::check())
                    <div class="dropdown-menu dropdown-menu-right">
                        <nav class="nav">
                            {{--<a href="page-profile.html" class="nav-link"><i class="icon ion-person"></i> View Profile</a>--}}
                            <a href="{{'fetchComplaints'}}" class="nav-link"><i class="fa fa-inbox"></i>  Complaints</a>
                            <a href="{{route('settings')}}" class="nav-link"><i class="fa fa-cog"></i>  Account Settings</a>
                            <a href="{{route('logUserOut')}}" class="nav-link"><i class="fa fa-arrow-right"></i>  Sign Out</a>
                            <div class="divider"></div>
                            <a href="{{route('complain')}}" target="_blank" class="nav-link"><i class="fa fa-link"></i>  Visit Complaint Form</a>
                        </nav>
                    </div><!-- dropdown-menu -->
                @endif
            </div><!-- dropdown -->
        </div><!-- header-right -->
    </div><!-- container -->
</div><!-- slim-header -->

<div class="slim-navbar">
    <div class="container">
        <ul class="nav">
            <li class="nav-item {{ (!empty($page) && $page == "index" ? 'active' : '') }}">
                <a class="nav-link" href="{{route('dashboard')}}">
                    <i class="fa fa-home"></i>
                    <span>Dashboard</span>
                </a>
            </li>
            <li class="nav-item {{ (!empty($page) && $page == "projects" ? 'active' : '') }}">
                <a class="nav-link" href="{{route('allProjects')}}">
                    <i class="fa fa-file"></i>
                    <span>Projects</span>
                </a>
            </li>

            {{--<li class="nav-item with-sub">
                <a class="nav-link" href="#" data-toggle="dropdown">
                    <i class="icon ion-ios-gear-outline"></i>
                    <span>Forms</span>
                </a>
                <div class="sub-item">
                    <ul>
                        <li><a href="form-elements.html">Form Elements</a></li>
                        <li><a href="form-layouts.html">Form Layouts</a></li>
                        <li><a href="form-validation.html">Form Validation</a></li>
                        <li><a href="form-wizards.html">Form Wizards</a></li>
                        <li><a href="form-editor.html">WYSIWYG Editor</a></li>
                        <li><a href="form-select2.html">Select2</a></li>
                        <li><a href="form-rangeslider.html">Range Slider</a></li>
                        <li><a href="form-datepicker.html">Datepicker</a></li>
                    </ul>
                </div><!-- dropdown-menu -->
            </li>--}}
            @if(Auth::check() && Auth::user()->role == "Admin")
            <li class="nav-item {{ (!empty($page) && $page == "users" ? 'active' : '') }}">
                <a class="nav-link" href="{{route('allUsers')}}">
                    <i class="fa fa-users"></i>
                    <span>App Users</span>
                    {{--<span class="square-8"></span>--}}
                </a>
            </li>
            @endif
            <li class="nav-item {{ (!empty($page) && $page == "settings" ? 'active' : '') }}">
                <a class="nav-link" href="{{route('settings')}}">
                    <i class="fa fa-cogs"></i>
                    <span>Settings</span>
                    {{--<span class="square-8"></span>--}}
                </a>
            </li>

        </ul>
    </div><!-- container -->
</div><!-- slim-navbar -->

<div class="container">
    <div class="row "> <div class="col-md-12"> <input type="hidden" value="{{route('complain')}}" id="formLink"> <button id="copy_url" class="btn btn-sm btn-primary mt-4 float-right">Copy Complaint Form url</button></div></div>
</div>
@yield('content')
{{--Modal for Adding a user --}}
<div id="editTask" class="modal fade" aria-hidden="true">
    <div class="modal-dialog modal-dialog-vertical-center" role="document">
        <div class="modal-content bd-0 tx-14">
            <div class="modal-header">
                <h6 class="tx-14 mg-b-0 tx-uppercase tx-inverse tx-bold">Add User</h6>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body pd-25">
                <input type="hidden" id="theid">
                <div class="form-group">
                    <label></label>
                    <input type="text" id="tskname" class="form-control">
                </div>
                <div class="form-group">
                    <label>Task Estimate Budget</label>
                    <input type="text" id="tskamt" class="form-control">
                </div>
                <div class="form-group">
                    <label>Task Description</label>
                    <textarea class="form-control" id="descrptn"></textarea>
                </div>
                <div class="form-group">
                    <label>Expected Start Date</label>
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <div class="input-group-text">
                                <i class="fa fa-calendar tx-16 lh-0 op-6"></i>
                            </div>
                        </div>
                        <input type="text" name="esdate" id="esdate" class="form-control fc-datepicker" data-provide='datepicker' data-date-container='#addTask' placeholder="YYYY-MM-DD">
                    </div>
                </div>
                <div class="form-group">
                    <label>Expected Start Date</label>
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <div class="input-group-text">
                                <i class="fa fa-calendar tx-16 lh-0 op-6"></i>
                            </div>
                        </div>
                        <input type="text" name="efdate" id="efdate" class="form-control fc-datepicker" data-provide='datepicker' data-date-container='#addTask' placeholder="YYYY-MM-DD">
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-success" id="editTsk">Edit</button>
            </div>
        </div>
    </div>
</div>
<div class="slim-footer" style="">
    <div class="container">
        <p>Copyright {{date('Y')}} &copy; All Rights Reserved. FMX | Project Tracker</p>
    </div><!-- container -->
</div><!-- slim-footer -->


<script src="{{asset('lib/jquery/js/jquery.js')}}"></script>
<script src="{{asset('lib/popper.js/js/popper.js')}}"></script>
<script src="{{asset('lib/bootstrap/js/bootstrap.js')}}"></script>
<script src="{{asset('lib/datatables/js/jquery.dataTables.js')}}"></script>
<script src="{{asset('lib/datatables-responsive/js/dataTables.responsive.js')}}"></script>
<script src="{{asset('lib/select2/js/select2.min.js')}}"></script>

<script src="{{asset('lib/jquery-ui/js/jquery-ui.js')}}"></script>
<script src="{{asset('lib/fontawesome/js/fontawesome.min.js')}}"></script>
<script src="{{asset('lib/jquery.cookie/js/jquery.cookie.js')}}"></script>
<script src="{{asset('lib/moment/js/moment.js')}}"></script>
<script src="{{asset('lib/jquery.steps/js/jquery.steps.js')}}"></script>
<script src="{{asset('lib/parsleyjs/js/parsley.js')}}"></script>
<script src="{{asset('lib/jquery-ui/js/jquery-ui.js')}}"></script>
<script src="{{asset('js/slim.js')}}"></script>
<script src="{{asset('js/custom.js')}}"></script>
@yield('script')
<script>
    $(document).on('click', '#copy_url', function () {
        document.getElementById('formLink').type = 'text';
        document.getElementById('formLink').select();
        document.execCommand("copy");
        if(document.execCommand("copy")){
            document.getElementById('formLink').type = 'hidden';
            $('#success').modal('toggle');
            let msg = "<p>URL Copied to be shared!.</p>";
            $('#successmsg').html(msg);
        }else{
            $('#error').modal('toggle');
            let msg = "<p>URL cannot be copied.</p>";
            $('#message').html(msg);
        }
    })
</script>
</body>
</html>