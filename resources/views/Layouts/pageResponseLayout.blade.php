<html lang="en"><!-- Mirrored from themepixels.me/slim1.1/template/page-503.html by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 02 Jun 2019 13:04:28 GMT --><head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Twitter -->
    <meta name="twitter:site" content="@themepixels">
    <meta name="twitter:creator" content="@themepixels">
    <meta name="twitter:card" content="summary_large_image">
    <meta name="twitter:title" content="Slim">
    <meta name="twitter:description" content="Premium Quality and Responsive UI for Dashboard.">
    <meta name="twitter:image" content="../../slim/img/slim-social.html">

    <!-- Facebook -->
    <meta property="og:url" content="http://themepixels.me/slim">
    <meta property="og:title" content="Slim">
    <meta property="og:description" content="Premium Quality and Responsive UI for Dashboard.">

    <meta property="og:image" content="../../slim/img/slim-social.html">
    <meta property="og:image:secure_url" content="../../slim/img/slim-social.html">
    <meta property="og:image:type" content="image/png">
    <meta property="og:image:width" content="1200">
    <meta property="og:image:height" content="600">

    <!-- Meta -->
    <meta name="description" content="Premium Quality and Responsive UI for Dashboard.">
    <meta name="author" content="ThemePixels">

    <title>FMX | Project Tracker</title>

    <!-- Vendor css -->
    <link href="{{asset('lib/fontawesome/css/fontawesome.css')}}" rel="stylesheet">
    <link href="{{asset('lib/fontawesome/css/all.css')}}" rel="stylesheet">

    <!-- Slim CSS -->
    <link rel="stylesheet" href="{{asset('css/slim.css')}}">

    <link id="headerSkin" rel="stylesheet" href=""></head>
<body>

<div class="page-error-wrapper">
    <div>
        @yield('content')
    </div>

</div><!-- perrorsrror-wrapper -->

<script src="{{asset('lib/jquery/js/jquery.js')}}"></script>
<script src="{{asset('lib/popper.js/js/popper.js')}}"></script>
<script src="{{asset('lib/bootstrap/js/bootstrap.js')}}"></script>

<script src="{{asset('js/slim.js')}}"></script>



<!-- Mirrored from themepixels.me/slim1.1/template/page-503.html by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 02 Jun 2019 13:04:28 GMT -->

</body>
</html>