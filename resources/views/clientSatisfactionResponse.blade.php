@extends('Layouts.pageResponseLayout')
@section('content')
    <h5 class="">Feedback Response</h5>
    @if($response == "satisfied")<h5 class="tx-sm-24 tx-normal">We appreciate you!</h5>
    @elseif($response == "unsatisfied")
    <h5 class="tx-sm-24 tx-normal">We aren't happen you are not satisfied, Your response is received and will be acted upon.</h5>
    @else
        <p>An error occurred! Please hit the button from your mail again to give us your feedback.ss</p>
    @endif
    {{--<p class="mg-b-50">You have successfully unlock the next stage of the task {{$task->task_name}}. You can login to view details.</p>--}}
    <p class="error-footer">© Copyright {{date('Y')}}. All Rights Reserved. FMX ProjectTracker.</p>
@endsection