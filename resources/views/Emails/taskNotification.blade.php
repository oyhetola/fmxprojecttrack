<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title></title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
</head>

<body>
<div class='box' style="margin: 0 auto; width: 600px;  top: 200px; left: 50%; transform: -50%, 50%; border: 2px solid #b24a13;">
    <div style="width:600px; height:60px; background-color:#b23415; color:white; font-family: Montserrat; padding-top:12px; text-align:center;"><h3>New Task Notification.</h3></div>
    <div style="text-align:center; align-content: center"><img src="" style="width: 400px; height:150px; padding-top: 15px;" alt="FMX Integrated Limited">
        <h2 style=" font-family: Helvetica Neue, Arial, Helvetica, sans-serif;"></h2>
    </div>
    <div style="font-family: Montserrat; text-align: left;  padding: 35px 30px; color: black">
        <p style="font-family: MontserratBlack; padding: 0 5px;">Hello Admin,</p>
        <p> A new task named {{$task->task_name}} has been created by {{$task->user->firstname}}</p>
        <p><h4>Other task details</h4></p>
        <p>Type: {{$task->project_type}}</p>
        <p>Amount: {{$task->amount_due}}</p>
        <p>Supervisor: {{$task->supervisor}}</p>
        <p>Vendor: {{$task->vendor}}</p>
        <br>
        {{--<p><a href="{{route('updateTaskStatus', ['taskId'=>$cont->task->id, 'stage'=>$newStage])}}" class="btn btn-primary" style="background-color: #b24514;--}}
        {{--padding: 15px 20px; width: 150px; color: white; border: 0; text-decoration: none;">End</a></p>--}}
        {{--<p><a href="{{route('viewLogin')}}" class="btn btn-primary" style="background-color: #b24514;--}}
{{--padding: 15px 20px; width: 150px; color: white; border: 0; text-decoration: none;">Login to check</a></p>--}}
        @if($task->project_type == "Ad-hoc")
            <p>This task requires approval. Please click the button below to approve.</p><br>
            <p><a href="{{route('approveTask', ['taskId'=>$task->id, 'project'=>$task->project_id])}}" class="btn btn-primary" style="background-color: #b24514; padding: 7px 5px">Approve</a>
        @elseif($task->project_type == "Routine" && $task->amt > 0)
            <p>This task requires approval. Please click the button below to approve.</p><br>
            <p><a href="{{route('approveTask', ['taskId'=>$task->id, 'project'=>$task->project_id])}}" class="btn btn-primary" style="background-color: #b24514;">Approve</a>
        @endif
        <br>
        <i><p><strong>Cheers!</strong></p>
            <strong>FMX Project Tracking App.</strong>
        </i>
    </div>
</div>
<!-- <button class="btn btn-primary" style="background-color: #20B2AA;
padding: 15px 3px; width: 150px; color: white; border: 0;">Verify Account</button>
</div> -->
</body>
</html>