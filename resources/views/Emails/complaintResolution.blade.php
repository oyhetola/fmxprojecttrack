<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title></title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
</head>

<body>
<div class='box' style="margin: 0 auto; width: 600px;  top: 200px; left: 50%; transform: -50%, 50%; border: 2px solid #b24a13;">
    <div style="width:600px; height:60px; background-color:#b23415; color:white; font-family: Montserrat; padding-top:12px; text-align:center;"><h3>Complaint Update</h3></div>
    <div style="text-align:center; align-content: center"><img src="" style="width: 400px; height:150px; padding-top: 15px;" alt="FMX Integrated Limited">
        <h2 style=" font-family: Helvetica Neue, Arial, Helvetica, sans-serif;"></h2>
    </div>
    <div style="font-family: Montserrat; text-align: left;  padding: 35px 30px; color: black">
        <p style="font-family: MontserratBlack;">Hello,</p>
        <p>  You lodged a complaint to us on {{date('jS F, Y', strtotime($complaint->created_at))}}.</p>
        <h4>Your Message</h4>
        <p>{!! nl2br($complaint->detailed_complaint) !!}</p>
        <br>
        @if($case == "resolved")
        <p>Your complaint has now been resolved.</p>
        <br>
        <p>Let us if you are satisfied</p>
            <a href="{{route('satisfied', ['id'=>$complaint->id])}}"><button style="padding: 10px 7px; background: green; color: white; cursor: pointer; border-radius: 0; border: 0px;" data-id="{{$complaint->id}}" class="satisfied">Yes I am satisfied</button></a>
            <a href="{{route('unsatisfied', ['id'=>$complaint->id])}}"><button style="padding: 10px 7px; background: red; color: white; cursor: pointer; border-radius: 0; border: 0px;" data-id="{{$complaint->id}}" class="dissatisfied">No, I am not</button></a>
        <br>
        @elseif($case == "unresolved")
            <p>The issue has just been indicated unresolved. We are doing everything to get it resolved.</p>
        @endif
        <i>
            <strong>Cheers!</strong>
            <strong>FMX Project Tracking App.</strong>
        </i>
    </div>
</div>
<!-- <button class="btn btn-primary" style="background-color: #20B2AA;
padding: 15px 3px; width: 150px; color: white; border: 0;">Verify Account</button>
</div> -->
</body>
{{--<script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>--}}
{{--<script src="{{asset('lib/jquery/js/jquery.js')}}"></script>--}}
{{--<script src="{{asset('lib/popper.js/js/popper.js')}}"></script>
<script src="{{asset('lib/bootstrap/js/bootstrap.js')}}"></script>--}}
<script>
    /*$('.satisfied').on('click', function () {
        let ths = $(this)
        let id =  $(this).data('id');
        let token = ""
        $.post('', {id:id, _token:token}, function (data) {
            if(data.success){
                ths.prop('disabled', true);
                ths.html('Success!');
                $('.unsatisfied').prop('disabled', true);
            }
        })
    });

    $('.dissatisfied').on('click', function () {
        let ths = $(this)
        let id =  $(this).data('id');
        let token = ""
        $.post('', {id:id, _token:token}, function (data) {
            if(data.success){
                ths.prop('disabled', true);
                ths.css('background', 'green');
                ths.html('Success!');
                $('.satisfied').prop('disabled', true);
            }
        })
    });*/
</script>
</html>