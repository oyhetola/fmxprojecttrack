<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title></title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
</head>

<body>
<div class='box' style="margin: 0 auto; width: 600px;  top: 200px; left: 50%; transform: -50%, 50%; border: 2px solid #b24a13;">
    <div style="width:600px; height:60px; background-color:#b23415; color:white; font-family: Montserrat; padding-top:12px; text-align:center;"><h3>Waiting For Approval.</h3></div>
    <div style="text-align:center; align-content: center"><img src="" style="width: 400px; height:150px; padding-top: 15px;" alt="FMX Integrated Limited">
        <h2 style=" font-family: Helvetica Neue, Arial, Helvetica, sans-serif;"></h2>
    </div>
    <div style="font-family: Montserrat; text-align: left;  padding: 35px 30px; color: black">
        <p style="font-family: MontserratBlack;">Hello,</p>
        <p>  {{ Auth::check() ? \Illuminate\Support\Facades\Auth::user()->firstname : 'Someone'}} requests approval for the project named <b>{{$project->name}}</b>. The project start date is
            {{date('jS F, Y', strtotime($project->start_date))}} and is due to end by {{date('jS F, Y', strtotime($project->finish_date))}}.
            The project budget is  <b>&#8358; {{$project->budget}}</b></p>
        <p>Hit the button below to approve the project.</p><br>
        <br>
        {{--<p><a href="{{route('updateTaskStatus', ['taskId'=>$cont->task->id, 'stage'=>$newStage])}}" class="btn btn-primary" style="background-color: #b24514;--}}

        @if($user->role=="Manager" && $project->budget <= 400000)
            <p><a href="{{route('approveProject', ['projectId'=>$project->id, 'user'=>$user->id])}}" class="btn btn-primary" style="background-color: #b24514;
padding: 15px 20px; width: 150px; color: white; border: 0; text-decoration: none;">Approve</a></p>
        @elseif($user->role=="Manager" && $project->user->role == "Admin")
            <p><a href="{{route('approveProject', ['projectId'=>$project->id, 'user'=>$user->id])}}" class="btn btn-primary" style="background-color: #b24514;
padding: 15px 20px; width: 150px; color: white; border: 0; text-decoration: none;">Approve</a></p>
        @endif
        @if($user->role=="Admin")
            <p><a href="{{route('approveProject', ['projectId'=>$project->id, 'user'=>$user->id])}}" class="btn btn-primary" style="background-color: #b24514;
padding: 15px 20px; width: 150px; color: white; border: 0; text-decoration: none;">Approve</a></p>
        @endif
        {{--, 'user'=>$user--}}
        <br>
        <i>
            <p><strong>Cheers!</strong></p>
            <strong>FMX Project Tracking App.</strong>
        </i>
    </div>
</div>
<!-- <button class="btn btn-primary" style="background-color: #20B2AA;
padding: 15px 3px; width: 150px; color: white; border: 0;">Verify Account</button>
</div> -->
</body>
</html>