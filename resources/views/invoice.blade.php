<!DOCTYPE html>
<html>
<head>
    {{--<link rel="stylesheet" href="{{asset('css/slim.css')}}">--}}
</head>
<div class="card card-invoice">
    <div class="card-body">
        <div class="invoice-header">
            <h1 class="invoice-title">Invoice</h1>
            <div class="billed-from">
                <h6>FMX Integrated Service Limited</h6>
                <p>Plot 883., Samuel Manuwa Street, VI, Lagos<br>
                    Tel No: 07034567854<br>
                    Email: m.nyamali@xlafricagroup.com</p>
            </div><!-- billed-from -->
        </div><!-- invoice-header -->

        <div class="row mg-t-20">
            <div class="col-md">
                <label class="section-label-sm tx-gray-500">Billed To</label>
                <div class="billed-to">
                        <p>Email: {{$task->client_email}}</p>
                </div>
            </div><!-- col -->
            <div class="col-md">
                <label class="section-label-sm tx-gray-500">Invoice Information</label>
                <p class="invoice-info-row">
                    <span>Invoice No:</span>
                    <span>FMX-00{{$task->id}}</span>
                </p>
                <p class="invoice-info-row">
                    <span>Project ID:</span>
                    <span>P/FMX/0{{$task->project->id}}</span>
                </p>
                <p class="invoice-info-row">
                    <span>Issue Date:</span>
                    <span>{{date('jS F, Y', strtotime(\Illuminate\Support\Carbon::now()))}}</span>
                </p>
                <p class="invoice-info-row">
                    <span>Due Date:</span>
                    <span>{{date('jS F, Y', strtotime($task->expected_finish_date))}}</span>
                </p>
            </div><!-- col -->
        </div><!-- row -->

        <div class="table-responsive mg-t-40">
            <table class="table table-invoice">
                <thead>
                <tr>
                    <th class="wd-20p">Title</th>
                    <th class="wd-40p">Description</th>
                    <th class="tx-right">Unit Price</th>
                    <th class="tx-right">Quantity</th>
                    <th class="tx-right">Amount</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>{{$task->name}}</td>
                    <td class="tx-12">{{$task->description}}</td>
                    <td class="tx-right">&#8358; {{$task->amount_due}}.00</td>
                    <td class="tx-right">1</td>
                    <td class="tx-right">{{$task->amount_due}}.00</td>
                </tr>

                <tr>
                    <td colspan="2" rowspan="4" class="valign-middle">
                        <div class="invoice-notes">
                            <label class="section-label-sm tx-gray-500">Notes</label>
                            <p>This is the quotation as agreed upon from the start of the project.</p>
                        </div><!-- invoice-notes -->
                    </td>
                    <td class="tx-right">Sub-Total</td>
                    <td colspan="2" class="tx-right">&#8358;{{$task->amount_due}}.00</td>
                </tr>
                <tr>
                    <td class="tx-right">Tax (5%)</td>
                    <td colspan="2" class="tx-right">287.50</td>
                </tr>
                {{--<tr>
                    <td class="tx-right">Discount</td>
                    <td colspan="2" class="tx-right">-$50.00</td>
                </tr>--}}
                <tr>
                    <td class="tx-right tx-uppercase tx-bold tx-inverse">Total Due</td>
                    <td colspan="2" class="tx-right"><h4 class="tx-primary tx-bold tx-lato">{{$task->amount_due}}</h4></td>
                </tr>
                </tbody>
            </table>
        </div><!-- table-responsive -->

        <hr class="mg-b-60">

        {{--<a href="#" class="btn btn-primary btn-block">Pay Now</a>--}}

    </div><!-- card-body -->
</div>
</html>