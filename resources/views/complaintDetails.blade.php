@extends('Layouts.dashboardLayout')
@section('content')
    <div>@include('includes.messages')</div>
    <div class="slim-mainpanel">
        <div class="container">
            <div class="slim-pageheader">
                <ol class="breadcrumb slim-breadcrumb">
                    <li class="breadcrumb-item"><a href="{{route('dashboard')}}">Dashbaord</a></li>
                    <li class="breadcrumb-item"><a href="{{route('fetchComplaints')}}">Complaints</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Complaint Details</li>
                </ol>
                <h6 class="slim-pagetitle">Complaints</h6>
            </div><!-- slim-pageheader -->

            <div class="section-wrapper">
                <label class="section-title">complaint Details - {{$complaint->title}}</label>
                {{--<p class="mg-b-20 mg-sm-b-40"> </p>--}}
                <div class="card-body">
                    @if($complaint->resolution_status == 0) <button class="btn btn-sm btn-success float-right resolved" data-id="{{$complaint->id}}">Mark as resolved</button> <p class="float-right"><i class="fa fa-circle text-danger"></i> Unresolved @else <button class="btn btn-sm btn-warning float-right unresolved" data-id="{{$complaint->id}}">Mark unresolved</button> <p class="float-right"><i class="fa fa-check text-success"></i> Resolved </p>@endif
                </div>

                <div class="card-body">
                    <div class="card-title">Title</div>
                    <p>{{$complaint->title}}</p>
                </div>
                <div class="card-body">
                    <div class="card-title">Message</div>
                    <p>{!! nl2br($complaint->detailed_complaint) !!}</p>
                </div>
                <div class="card-body">
                    <div class="card-title">Location</div>
                    <p>{{$complaint->location}}</p>
                </div>
                <div class="card-body">
                    <div class="card-title">Client Satisfaction</div>
                    <p>{{$complaint->client_satisfaction == 1 ? 'Satisfied' : 'Unsatisfied'}}</p>
                </div>
                <div class="card-body">
                    <div class="card-title">Location</div>
                    <p>{{$complaint->location}}</p>
                </div>
                <div class="card-body">
                    <div class="card-title">Date of defect was noticed</div>
                    <p>{{$complaint->date}}</p>
                </div>
                <div class="card-body">
                    <div class="card-title">Time</div>
                    <p>{{$complaint->time}}</p>
                </div>
                <div class="card-body">
                    <div class="card-title">Witness Name</div>
                    <p>{{$complaint->witness_name ? $complaint->witness_name : '-'}}</p>
                </div>
                <div class="card-body">
                    <div class="card-title">Witness Email</div>
                    <p>{{$complaint->witness_email ? $complaint->witness_email : '-'}}</p>
                </div>
                <div class="card-body">
                    <div class="card-title">Witness Contact Number</div>
                    <p>{{$complaint->witness_phone ? $complaint->witness_phone : '-'}}</p>
                </div>
            </div>
        </div>
    </div>


    {{--edit prloject--}}

    {{--edit project--}}
    @include('includes.alerts')
@endsection
@section('script')
    <script>
        $.ajaxSetup({
            headers:{'X-CSRF-TOKEN': $('meta[name=csrf-token]').attr('content')}
        });

        $('.resolved').on('click', function () {
            if(confirm('Are you sure this is really resolved? The complainant would get a mail to confirm this.')){
                p = $(this)
                let dataToSend = {id: p.data('id')};
                p.prop('disabled',true );
                $.ajax({
                    'url': '{{route('toggleResolve')}}',
                    'method': "POST",
                    data: dataToSend,
                    //'contentType': 'application/json'
                }).done( function(data){
                    if(data.success){
                        p.prop('disabled', false);
                        p.removeClass('resolved');
                        p.addClass('btn-warning');
                        p.removeClass('btn-success');
                        p.addClass('unresolved');
                        p.html('Mark Unresolved');
                        p.next().html('<i class="fa fa-check text-success"></i> Resolved')
                        $('#success').modal('toggle');
                        let msg = "<p>You successfully indicated the complaint resolved.</p>";
                        $('#successmsg').html(msg);
                    }else{
                        $('#error').modal('toggle');
                        let msg = "<p>An Error occurred.</p>";
                        $('#message').html(msg);
                    }
                });
            }
        })

        $('.unresolved').on('click', function () {
            if(confirm('Are you sure you want to mark this unresolved?')){
                p = $(this)
                let dataToSend = {id: p.data('id')};
                p.prop('disabled',true );
                $.ajax({
                    'url': '{{route('unResolve')}}',
                    'method': "POST",
                    data: dataToSend,
                    //'contentType': 'application/json'
                }).done( function(data){
                    if(data.success){
                        p.prop('disabled', false);
                        p.removeClass('unresolved');
                        p.addClass('btn-success');
                        p.removeClass('btn-warning');
                        p.addClass('resolved');
                        p.html('Mark as resolved');
                        p.next().html('<i class="fa fa-circle text-danger"></i> Unresolved')
                        $('#success').modal('toggle');
                        let msg = "<p>You successfully indicated the complaint unresolved.</p>";
                        $('#successmsg').html(msg);
                    }else{
                        $('#error').modal('toggle');
                        let msg = "<p>An Error occurred.</p>";
                        $('#message').html(msg);
                    }
                });
            }

        })
    </script>

@endsection('script')