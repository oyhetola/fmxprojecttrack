@extends('Layouts.pageResponseLayout')
@section('content')
    <h1 class="error-title">505</h1>
    <h5 class="tx-sm-24 tx-normal">An error Occurred.</h5>
    <p class="mg-b-50">An unexpected error occurred while trying to carry out this action.</p>
    <p class="mg-b-50">@if(\Illuminate\Support\Facades\Auth::check()) <a href="{{route('viewLogin')}}" class="btn btn-error">Login</a> @else <a href="{{route('dashboard')}}" class="btn btn-success">Login</a> @endif</p>
    <p class="error-footer">© Copyright {{date('Y')}}. All Rights Reserved. FMX ProjectTracker.</p>
@endsection