@extends('Layouts.pageResponseLayout')
@section('content')
    <h1 class="error-title">404</h1>
    <h5 class="tx-sm-24 tx-normal">Page not Found.</h5>
    <p class="mg-b-50">This page you are looking for is not found.</p>
    <p class="mg-b-50">@if(Auth::check()) <a href="{{route('viewLogin')}}" class="btn btn-error">Login</a> @else <a href="{{route('dashboard')}}" class="btn btn-error">Login</a> @endif</p>
    <p class="error-footer">© Copyright {{date('Y')}}. All Rights Reserved. FMX ProjectTracker.</p>
@endsection