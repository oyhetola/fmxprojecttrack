@extends('Layouts.dashboardLayout')
@section('content')
    <div>@include('includes.messages')</div>
    <div class="slim-mainpanel">
        <div class="container">
            <div class="slim-pageheader">
                <ol class="breadcrumb slim-breadcrumb">
                    <li class="breadcrumb-item"><a href="{{route('dashboard')}}">Dashbaord</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Complaints</li>
                </ol>
                <h6 class="slim-pagetitle">Complaints</h6>
            </div><!-- slim-pageheader -->

            <div class="section-wrapper">
                <label class="section-title">All complaints from various assumed clients</label>
                {{--<p class="mg-b-20 mg-sm-b-40"> </p>--}}

                <div class="table-wrapper">
                    <table id="datatable1" class="table display table-responsive">
                        <!-- no-wrap  -->
                        <thead>
                        <tr>
                            <th class="">Title</th>
                            <!-- wd-15p-force -->
                            <th class="">Message</th>
                            <th class="">Company</th>
                            <!-- wd-10p-force -->
                            <th class="">Location</th>
                            <!-- wd-10p-force -->
                            <th class="">Complainant</th>
                            <!-- wd-15p-force -->
                            <th>Resolution Status</th>
                            <th>Customer Satisfaction</th>
                            <th>Action</th>
                            <!-- "wd-30p-force" -->
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($complaints as $complaint)
                            <tr>
                                <td>{{$complaint->title}}</td>
                                <td>{!!  nl2br($complaint->detailed_complaint) !!}</td>
                                <td> {{$complaint->company}}</td>
                                <td>{{$complaint->location}}</td>
                                <td>{{$complaint->complainants_name}} ({{$complaint->email}})</td>
                                <td>@if($complaint->resolution_status == false) <button class="btn btn-sm btn-success resolved" data-id="{{$complaint->id}}">Mark as Resolved</button><p><i class="fa fa-circle text-danger"></i> Unresolved</p> @else <button class="btn btn-sm btn-warning unresolved" data-id="{{$complaint->id}}">Mark unresolved</button><p><i class="fa fa-check text-success"></i> Resolved</p> @endif</td>
                                <td>{{$complaint->client_satisfaction == 1 ? 'Satisfied' : 'Unsatisfied'}}</td>
                                <td><a href="{{route('complaintDetails', [$complaint->id])}}" class="btn btn-sm btn-info">View Details<i class="fa fa-eye"></i></a> </td>
                            </tr>
                        @endforeach
                        {{-- <buttton class="btn btn-info btn-sm">Mark as Completed</buttton>--}}
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>


    {{--edit prloject--}}
    <div id="editProject" class="modal fade" aria-hidden="true" style="">
        <div class="modal-dialog modal-dialog-vertical-center" role="document">
            <div class="modal-content bd-0 tx-14">
                <div class="modal-header pd-y-20 pd-x-25">
                    <h6 class="tx-14 mg-b-0 tx-uppercase tx-inverse tx-bold">Edit Project</h6>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body pd-25">
                    <h5 class="lh-3 mg-b-20"><a href="#" class="tx-inverse hover-primary"> Fill in the details to edit project <div id="pjname"></div></a></h5>
                    <input name="theid" type="hidden">
                    <div class="form-group">
                        <label>Project Name</label>
                        <input class="form-control" type="text" name="projectname" placeholder="Project Name">
                    </div>
                    {{--@if(Auth::user()->role == "Admin")--}}
                    <div class="form-group">
                        <label>Budget</label>
                        {{--<textarea class="form-control" id="description"></textarea>--}}
                        <input class="form-control" name="bgt">
                    </div>
                    {{--@endif--}}
                    <div class="form-group">
                        <label>Client</label>
                        <select class="form-control" name="clnt">
                            <option>All</option>
                            {{--@foreach($clients as $client)
                                <option data-id="{{$client->id}}">{{$client->name}}</option>
                            @endforeach--}}
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Expected Start Date</label>
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <div class="input-group-text">
                                    <i class="fa fa-calendar tx-16 lh-0 op-6"></i>
                                </div>
                            </div>
                            <input type="text" name="pstartdate" id="pstartdate" class="form-control fc-datepicker" data-provide='datepicker' data-date-container='#addTask' placeholder="YYYY-MM-DD">
                        </div>
                    </div>
                    <div class="form-group">
                        <label>Expected End Date</label>
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <div class="input-group-text">
                                    <i class="fa fa-calendar tx-16 lh-0 op-6"></i>
                                </div>
                            </div>
                            <input type="text" name="penddate" class="form-control fc-datepicker" placeholder="YYYY-MM-DD" id="penddate">
                        </div>
                    </div>
                    <div class="ml-2" id="docContainer">

                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" id="doEdit" class="btn btn-primary">Edit Project</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div><!-- modal-dialog -->
    </div>
    {{--edit project--}}
    @include('includes.alerts')
@endsection
@section('script')
<script>
    $.ajaxSetup({
        headers:{'X-CSRF-TOKEN': $('meta[name=csrf-token]').attr('content')}
    });

    $('.resolved').on('click', function () {
        if(confirm('Are you sure this is really resolved? The complainant would get a mail to confirm this.')){
            p = $(this)
            let dataToSend = {id: p.data('id')};
            p.prop('disabled',true );
            $.ajax({
                'url': '{{route('toggleResolve')}}',
                'method': "POST",
                data: dataToSend,
                //'contentType': 'application/json'
            }).done( function(data){
                if(data.success){
                    p.prop('disabled', false);
                    p.removeClass('resolved');
                    p.addClass('btn-warning');
                    p.removeClass('btn-success');
                    p.addClass('unresolved');
                    p.html('Mark Unresolved');
                    p.next().html('<i class="fa fa-check text-success"></i> Resolved')
                    $('#success').modal('toggle');
                    let msg = "<p>You successfully indicated the complaint resolved.</p>";
                    $('#successmsg').html(msg);
                }else{
                    $('#error').modal('toggle');
                    let msg = "<p>An Error occurred.</p>";
                    $('#message').html(msg);
                }
            });
        }
    })

    $('.unresolved').on('click', function () {
        if(confirm('Are you sure you want to mark this unresolved?')){
            p = $(this)
            let dataToSend = {id: p.data('id')};
            p.prop('disabled',true );
            $.ajax({
                'url': '{{route('unResolve')}}',
                'method': "POST",
                data: dataToSend,
                //'contentType': 'application/json'
            }).done( function(data){
                if(data.success){
                    p.prop('disabled', false);
                    p.removeClass('unresolved');
                    p.addClass('btn-success');
                    p.removeClass('btn-warning');
                    p.addClass('resolved');
                    p.html('Mark as resolved');
                    p.next().html('<i class="fa fa-circle text-danger"></i> Unresolved')
                    $('#success').modal('toggle');
                    let msg = "<p>You successfully indicated the complaint unresolved.</p>";
                    $('#successmsg').html(msg);
                }else{
                    $('#error').modal('toggle');
                    let msg = "<p>An Error occurred.</p>";
                    $('#message').html(msg);
                }
            });
        }

    })
</script>
@endsection('script')