@extends('Layouts.dashboardLayout')
@section('content')
    <div>@include('includes.messages')</div>
    <div class="slim-mainpanel">
        <div class="container">
            <div class="slim-pageheader">
                <ol class="breadcrumb slim-breadcrumb">
                    <li class="breadcrumb-item"><a href="{{route('dashboard')}}">Dashbaord</a></li>
                    <li class="breadcrumb-item active" aria-current="page">All Projects</li>
                </ol>
                <h6 class="slim-pagetitle">All Projects</h6>
            </div><!-- slim-pageheader -->

            <div class="section-wrapper">
                <label class="section-title">List of all projects in the system</label>
                <p class="mg-b-20 mg-sm-b-40">Projects Have tasks contained in them.  The tasks are tracked </p>

                <div class="table-wrapper">
                    <table id="datatable1" class="table display table-responsive">
                        <!-- no-wrap  -->
                        <thead>
                        <tr>
                            <th class="">Project name</th>
                            <!-- wd-15p-force -->
                            <th class="">Expected Start Time</th>
                            <th class="">Budget</th>
                            <!-- wd-10p-force -->
                            <th class="">Expected End Time</th>
                            <!-- wd-10p-force -->
                            <th class="">Opened By</th>
                            <!-- wd-15p-force -->
                            <th>completion Status</th>
                            <th>Action</th>
                            <!-- "wd-30p-force" -->
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($projects as $project)
                        <tr>
                            <td>{{$project->name}}</td>
                            <td>{{$project->start_date}}</td>
                            <td> @if($project->budget)&#8358; {{$project->budget}}@endif</td>
                            <td>{{$project->finish_date}}</td>
                            <td>{{$project->user->email}}</td>
                            <td>@if($project->completion_status == false) Incompleted @else Completed @endif</td>
                            <td><a href="{{route('allTasks', [$project->id])}}" class="btn btn-sm btn-info" title="View Project Tasks" data-placement="top" data-toggle="tooltip"><i class="fa fa-eye"></i></a> <button  class="btn btn-success btn-sm launch" title="Add task to project" data-placement="top" data-client="{{$project->client}}" data-pname="{{$project->name}}" data-id="{{$project->id}}" data-target="#createTask" data-toggle="modal" {{($project->completion_status ? 'disabled':'')}}><i class="fa fa-plus"></i></button> <button class="btn btn-sm btn-warning endProject" title="End Project" data-placement="top" data-id="{{$project->id}}" data-toggle="tooltip" {{($project->completion_status ? 'disabled':'')}}><i class="fa fa-hourglass-end"></i></button> <button class="btn-sm btn-default editp" data-id="{{$project->id}}" data-psdate="{{$project->start_date}}" data-client="{{$project->client}}" data-budget="{{$project->budget}}" data-pfdate="{{$project->finish_date}}" data-name="{{$project->name}}" title="editproject" data-doc="{{$project->document}}" data-toggle="modal" data-target="#editproject" {{($project->completion_status ? 'disabled':'')}}><i class="fa fa-edit"></i></button>@if($project->document != null && $project->document != "" ) <a href="{{route('downloadfile', ['idEnt1f1'=>$project->id, 'link'=>$project->document])}}">Project Document</a>@endif @if($project->approval_status == 0) <a href="/requestApproval/{{$project->id}}">Request Approval</a>@endif</td>
                        </tr>
                        @endforeach
                        {{-- <buttton class="btn btn-info btn-sm">Mark as Completed</buttton>--}}
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    {{--Create task modal--}}
    <div id="createTask" class="modal fade" aria-hidden="true" style="">
        <div class="modal-dialog modal-dialog-vertical-center" role="document">
            <div class="modal-content bd-0 tx-14">
                <div class="modal-header pd-y-20 pd-x-25">
                    <h6 class="tx-14 mg-b-0 tx-uppercase tx-inverse tx-bold">Add Task</h6>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body pd-25">
                    <h5 class="lh-3 mg-b-20"><a href="#" class="tx-inverse hover-primary"> Fill in the details to add task to project <div id="taskName"></div></a></h5>
                    <input type="hidden" name="keepProjectId">
                    <div class="form-group">
                        <label>Task Name</label>
                        <input class="form-control" type="text" name="taskname" placeholder="Task Name">
                    </div>
                    <div class="form-group">
                        <label>Description</label>
                        <textarea class="form-control" id="description"></textarea>
                    </div>
                    <div class="form-group">
                        <label>Expected Start Date</label>
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <div class="input-group-text">
                                    <i class="fa fa-calendar tx-16 lh-0 op-6"></i>
                                </div>
                            </div>
                            <input type="text" name="sdate" id="sdate" class="form-control fc-datepicker" data-provide='datepicker' data-date-container='#addTask' placeholder="YYYY-MM-DD">
                        </div>
                    </div>
                    <div class="form-group">
                        <label>Expected End Date</label>
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <div class="input-group-text">
                                    <i class="fa fa-calendar tx-16 lh-0 op-6"></i>
                                </div>
                            </div>
                            <input type="text" name="edate" class="form-control fc-datepicker" placeholder="YYYY-MM-DD" id="edate">
                        </div>
                    </div>
                    <div class="form-group">
                        <label>Select Project type</label>
                        <select class="form-control" name="projectType">
                            <option selected disabled>Select an Option</option>
                            <option>Routine</option>
                            <option>Ad-hoc</option>
                        </select>
                    </div>

                    <div id="baseDecider" class="doNotShow">
                        <div class="form-group">
                            <label>Quotation Prepared and Sent to client?</label>
                            <select class="form-control" name="quotationSent">
                                <option selected disabled>Select an Option</option>
                                <option value="1">Yes</option>
                                <option value="0">No</option>
                                <option value="0">No, I want to Continue</option>
                            </select>
                        </div>
                        <div id="dependentDiv" class="doNotShow">

                            <div class="form-group">
                                <label>Select Client</label>
                                <div class="row"><div class="col-md-10">
                                        <select class="form-control" name="cltname">
                                            <option selected disabled>All</option>
                                            @foreach($clients as $client)
                                                <option data-id="{{$client->id}}">{{$client->name}}</option>
                                            @endforeach
                                        </select>
                                    </div><div class="col-md-2"></div></div>
                            </div>
                            <div class="form-group">
                                <label>Client's email address</label>
                                <div class="row"><div class="col-md-10"><input class="form-control" name="cltemail" type="email"></div><div class="col-md-2"><i class="fa fa-circle indicator"></i></div></div>
                            </div>
                            <div id="dependentOnAdhoc" class="doNotShow">
                                <div class="form-group">
                                    <label>Select Supervisor</label>
                                    <select class="form-control" name="supervisor">
                                        <option disabled selected>Select an option</option>
                                        @foreach($supervisors as $supervisor)
                                            <option data-id="{{$supervisor->id}}">{{$supervisor->firstname}} {{$supervisor->lastname}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Select Vendor</label>
                                    <select class="form-control" name="vendor">
                                        <option disabled selected>Select an option</option>
                                        @foreach($vendors as $vendor)
                                            <option data-id="{{$vendor->id}}">{{$vendor->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label>Quotation Approved?</label>
                                <select class="form-control" name="approval">
                                    <option selected disabled>Select an Option</option>
                                    <option value="1">Yes</option>
                                    <option value="0">No</option>
                                    <option value="0">No, I want to Continue</option>
                                </select>
                            </div>

                            <div class="form-group doNotShow" id="tempHidden">
                                <label>Estimated Amount in Quatation</label>
                                <input type="text" name="amt" class="form-control">
                            </div>
                            <div class="form-group doNotShow" id="elseShow">
                                {{--<label>Estimated Amount in Quatation</label>--}}
                            </div>
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" id="addTask" class="btn btn-primary" disabled>Add Task</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div><!-- modal-dialog -->
    </div>
    {{--create task modal end--}}

    {{--edit prloject--}}
    <div id="editProject" class="modal fade" aria-hidden="true" style="">
        <div class="modal-dialog modal-dialog-vertical-center" role="document">
            <div class="modal-content bd-0 tx-14">
                <div class="modal-header pd-y-20 pd-x-25">
                    <h6 class="tx-14 mg-b-0 tx-uppercase tx-inverse tx-bold">Edit Project</h6>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body pd-25">
                    <h5 class="lh-3 mg-b-20"><a href="#" class="tx-inverse hover-primary"> Fill in the details to edit project <div id="pjname"></div></a></h5>
                    <input name="theid" type="hidden">
                    <div class="form-group">
                        <label>Project Name</label>
                        <input class="form-control" type="text" name="projectname" placeholder="Project Name">
                    </div>
                    {{--@if(Auth::user()->role == "Admin")--}}
                    <div class="form-group">
                        <label>Budget</label>
                        {{--<textarea class="form-control" id="description"></textarea>--}}
                        <input class="form-control" name="bgt">
                    </div>
                    {{--@endif--}}
                    <div class="form-group">
                        <label>Client</label>
                        <select class="form-control" name="clnt">
                            <option>All</option>
                            @foreach($clients as $client)
                                <option data-id="{{$client->id}}">{{$client->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Expected Start Date</label>
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <div class="input-group-text">
                                    <i class="fa fa-calendar tx-16 lh-0 op-6"></i>
                                </div>
                            </div>
                            <input type="text" name="pstartdate" id="pstartdate" class="form-control fc-datepicker" data-provide='datepicker' data-date-container='#addTask' placeholder="YYYY-MM-DD">
                        </div>
                    </div>
                    <div class="form-group">
                        <label>Expected End Date</label>
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <div class="input-group-text">
                                    <i class="fa fa-calendar tx-16 lh-0 op-6"></i>
                                </div>
                            </div>
                            <input type="text" name="penddate" class="form-control fc-datepicker" placeholder="YYYY-MM-DD" id="penddate">
                        </div>
                    </div>
                    <div class="ml-2" id="docContainer">

                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" id="doEdit" class="btn btn-primary">Edit Project</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div><!-- modal-dialog -->
    </div>
    {{--edit project--}}
    @include('includes.alerts')
@endsection
@section('script')
    <script>
        $.ajaxSetup({
            headers:{'X-CSRF-TOKEN': $('meta[name=csrf-token]').attr('content')}
        });

        $('#sdate').datepicker({
            showOtherMonths: true,
            selectOtherMonths: true,
            dateFormat: 'yy-mm-dd',
            changeMonth: true,
            changeYear: true
        });

        $('#edate').datepicker({
            showOtherMonths: true,
            selectOtherMonths: true,
            dateFormat: 'yy-mm-dd',
            changeMonth: true,
            changeYear: true
        });

        $('#pstartdate').datepicker({
            showOtherMonths: true,
            selectOtherMonths: true,
            dateFormat: 'yy-mm-dd',
            changeMonth: true,
            changeYear: true
        });

        $('#penddate').datepicker({
            showOtherMonths: true,
            selectOtherMonths: true,
            dateFormat: 'yy-mm-dd',
            changeMonth: true,
            changeYear: true
        });

        let projectId = "";

        $('input[name=amt]').bind('keyup input paste', function(){
            this.value = this.value.replace(/[^0-9,.]/g, '');
            if(this.value !== "" && $(this).val() !== null){
                $('#addTask').attr('disabled', false);
            }else{
                $('#addTask').attr('disabled', true);
            }
        });

        $('input[name=bgt]').bind('keyup input paste', function(){
            this.value = this.value.replace(/[^0-9,.]/g, '');
            /*if(this.value !== "" && $(this).val() !== null){
                $('#addTask').attr('disabled', false);
            }else{
                $('#addTask').attr('disabled', true);
            }*/
        });

        function validateEmail(email) {
            var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
            if(emailReg.test( email )){
                return true;
            }else{
                return false;
            }
        }

        $('input[name=cltemail]').bind('keyup blur', function () {
            validateEmail(this.value) ? $('.indicator').addClass('correct') : $('.indicator').addClass('error');
        });


        $('#datatable1').DataTable({
            //responsive: true,
            language: {
                searchPlaceholder: 'Search...',
                sSearch: '',
                lengthMenu: '_MENU_ items/page',
            }
        });

        $('select[name=quotationSent]').on('change', function () {
            if($('select[name=quotationSent] option:selected').text() == "Yes" || $('select[name=quotationSent] option:selected').text() == "No, I want to Continue"){
                $('#dependentDiv').removeClass('doNotShow');
                let r = $('select[name=cltname]').find('option:selected').data('id');
                if(r != null && r != "All"){
                    $.post('/findemail', {id:r}, function (data) {
                        $('input[name=cltemail]').val(data);
                    });
                }else{
                    $('input[name=cltemail]').val('');
                }
            }else{
                $('#dependentDiv').addClass('doNotShow');
            }
        });

        $('select[name=projectType]').on('change', function () {
            if($('select[name=projectType] option:selected').text() == "Routine"){

            }else{

            }
            if($('select[name=projectType] option:selected').text() == "Routine" ){
                $('#baseDecider').removeClass('doNotShow');
                $('#dependentOnAdhoc').addClass('doNotShow');
                $('input[name=amt]').val(0);
                $('#addTask').attr('disabled', false);
            }else if($('select[name=projectType] option:selected').text() == "Ad-hoc"){
                $('#baseDecider').removeClass('doNotShow');
                $('#dependentOnAdhoc').removeClass('doNotShow');
                $('input[name=amt]').val('');
                $('#addTask').attr('disabled', true);
            }
        });

        $('select[name=approval]').on('change', function () {
            if($('select[name=approval] option:selected').text() == "Yes" || $('select[name=approval] option:selected').text() == "No, I want to Continue"){
                $('#tempHidden').removeClass('doNotShow');
                $('#elseShow').addClass('doNotShow');
                /*$.post('/checkIfApproved', {id:projectId}, function (data) {
                    console.log(data);
                    if(data.approved){
                        $('#tempHidden').removeClass('doNotShow');
                        $('#elseShow').addClass('doNotShow');
                    }else if(data.notApproved){
                        $('#tempHidden').addClass('doNotShow');
                        $('#elseShow').removeClass('doNotShow');
                        $('#elseShow').empty();
                        $('#elseShow').append('<a href="requestApproval/'+projectId+'">Request Approval</a>');
                    }
                })*/
                if($('select[name=projectType] option:selected').text() == "Routine"){
                    $('input[name=amt]').val(0);
                    $('#addTask').attr('disabled', false);
                }else{
                    $('input[name=amt]').val('');
                    $('#addTask').attr('disabled', true);
                }
            }else{
                $('#tempHidden').addClass('doNotShow');
                $('#elseShow').removeClass('doNotShow');
                $('#elseShow').empty();
                $('#elseShow').append('<a href="requestApproval/'+projectId+'">Request Approval</a>')
            }
        });


        $(document).on('click', '.launch', function () {
            projectId = $(this).data('id');
            let client = $(this).data('client');
            $('#tempHidden').addClass('doNotShow');
            $('#elseShow').addClass('doNotShow');

            $('select[name=approval] option').filter(function() {
                return ($(this).text() == 'Select an Option');
            }).prop('selected', true);
            $('select[name=cltname] option').filter(function() {
                return ($(this).text() == client);
            }).prop('selected', true);
            $('#taskName').text($(this).data('pname'))
        });

        $('select[name=cltname]').on('change', function(){
//            let r = $(this).find('option:selected').val();
            let r = $(this).find('option:selected').data('id');
            if(r != null){
                $.post('/findemail', {id:r}, function (data) {
                    $('input[name=cltemail]').val(data);
                });
            }else{
                $('input[name=cltemail]').val('');
            }

        });
        
        $(document).on('click', '#addTask', function(){
            let thisbutton = $(this);
            thisbutton.attr('disabled', true);
            let tname = $('input[name=taskname]').val();
            let startdate = $('input[name=sdate]').val();
            let enddate = $('input[name=edate]').val();
            let qsent = $('select[name=quotationSent] option:selected').val();
            let approved = $('select[name=approval] option:selected').val();
            let amt = $('input[name=amt]').val();
            let cltemail = $('input[name=cltemail]').val();
            let cltname = $('select[name=cltname] option:selected').val();
            let description = $('#description').val();
            let projectType = $('select[name=projectType] option:selected').val();
            let vendor = $('select[name=vendor] option:selected').val();
            let supervisor = $('select[name=supervisor] option:selected').val();
//            if(amt == 0){
               /* $('#error').modal('toggle');
                let msg = "<p>Amount cannot be 0.</p><p>Provide a valid figure.</p>";
                $('#message').html(msg);*/
//            }else{
                if(tname == null  || startdate == "" || startdate == null || enddate == null || cltemail == null){
                    $('#error').modal('toggle');
                    let msg = "<p>Please Provide a name for your task.</p><p>Start Date cannot be empty.</p><p>End date cannot be empty.</p><p>Client's email is compulsory.</p>";
                    $('#message').html(msg);
                    thisbutton.attr('disabled', false);
                }else{
                    let postParameter = {taskname:tname, projectid:projectId, sdate:startdate, description:description, edate:enddate, projectType:projectType, quotationSent: qsent, quotationApproved:approved, cltname:cltname, cltemail:cltemail, amt:amt, vendor:vendor, supervisor:supervisor}
                    $.post("{{route('addTask')}}", postParameter, function(data){
                        console.log(data);
                        thisbutton.attr('disabled', false);
                        if(data.task){
                            $('#createTask').modal('toggle');
                            $('input').each(function () {
                                $(this).val('');
                            });
                            $('#success').modal('toggle');
                            let msg = "<p>You successfully created the task named "+ data.task.task_name+" <a class='btn btn-sm btn-info' href='/tasks/"+projectId+"'>View</a></p>";
                            $('#successmsg').html(msg);
                        }else{
                            $('#error').modal('toggle');
                            let msg = "<p>A problem occured while creating the task. Please try again.</p>";
                            $('#message').html(msg);
                        }
                    });
                }
//            }

        });

        $(document).on('click', '.editp', function () {
            let ths = $(this);
            $('input[name=projectname]').val($(this).data('name'));
            $('input[name=pstartdate]').val($(this).data('psdate'));
            $('input[name=penddate]').val($(this).data('pfdate'));
            $('input[name=theid]').val($(this).data('id'));
            $('input[name=bgt]').val($(this).data('budget'));
            $('select[name=clnt]').find('option').each(function () {
                if($(this).text() == ths.data('client')){
                    $(this).prop('selected', true);
                }
            });
            $('#pjname').text(ths.data('name'));
            if(ths.data('doc') != "undefined" && ths.data('doc') != ""){
                $('#docContainer').empty()
                $('#docContainer').append("<a href='' onclick='return false'>"+ths.data('doc')+"</a>  <b class='pull-right removeDoc' data-id='"+ths.data('id')+"' data-name='"+ths.data('doc')+"' style='cursor: pointer; color: red'><i class='fa fa-times'></i>Remove</b><div class='spinner-border spn d-none'></div>")
            }else {
                $('#docContainer').empty()
                $('#docContainer').append("<input type='file' data-id='"+ths.data('id')+"' name='editWithFile' accept='.pdf,.doc,.docx,.xlsx,.xls,.txt,.ppt,.pptx'> <div class='spinner-border spn d-none'></div>");
            }

            $('#editProject').modal('toggle');

        });

        $('#doEdit').on('click', function(){
            var sendthis = {id:$('input[name=theid]').val(), name: $('input[name=projectname]').val(), startdate: $('input[name=pstartdate]').val(), enddate: $('input[name=penddate]').val(), client:$('select[name=clnt] option:selected').val(), budget: $('input[name=bgt]').val()}
            $(this).attr('disabled', true);
            $.post("{{route('editproject')}}", sendthis, function (data) {
                console.log(data);
                $('#doEdit').attr('disabled', false);
                if(data = "success"){
                    $('#success').modal('toggle');
                    let msg = "<p><b>You successfully edited this project!</b></p>";
                    location.reload();
                    $('#successmsg').html(msg);
                }else if(data = "error"){
                    $('#error').modal('toggle');
                    let msg = "<p><b>Error occurred while trying to edit the project.</b></p>";
                    $('#message').html(msg);
                }
            });
        });
        
        $(document).on('click', '.endProject', function () {
            let id = $(this).data('id');
            let ths = $(this);
            if(confirm('Are you sure you want to end this project?')){
                ths.attr('disabled', true);
                $.post("{{route('endProject')}}", {id:id}, function (data) {
                if(data.success){
                    $('#success').modal('toggle');
                    let msg = "<p><b>You successfully ended this project!</b></p>";
                    $('#successmsg').html(msg);
                    ths.closest('tr').find('td:nth-child(5)').text('Completed');
                }else if(data.error){
                    ths.attr('disabled', false);
                    $('#error').modal('toggle');
                    let msg = "<p><b>The system noticed one or more unfinished task(s) in this project. You cannot end the project at the moment.</b></p>";
                    $('#message').html(msg);
                }else{
                    ths.attr('disabled', false);
                    $('#error').modal('toggle');
                    let msg = "<p>There was an error. Please try again.</p>";
                    $('#message').html(msg);
                }
            });
            }
        });

        $(document).on('click', '.removeDoc', function () {
            if(confirm('Are you sure you want to delete this file?')){
                let ths = $(this)
                $(this).addClass('d-none');
                $('.spn').removeClass('d-none');
                let id = $(this).data('id');
                let name = $(this).data('name');
                $.post('{{route('deletedocfile')}}', {id:id, name:name}, function (data) {
                    console.log(data);
                    if(data.success){
                        $('#docContainer').empty();
                        $('#docContainer').append("<input type='file' data-id='"+id+"' name='editWithFile' accept='.pdf,.doc,.docx,.xlsx,.xls,.txt,.ppt,.pptx'> <b class='d-none'></b>");
                    }else if(data.error){
                        ths.removeClass('d-none');
                        $('.spn').addClass('d-none');
                        $('#error').modal('toggle');
                        let msg = "<p>"+data.error+"</p>";
                        $('#message').html(msg);
                    }
                })
            }
        });

        $(document).on('change', 'input[name=editWithFile]', function () {
            $('#doEdit').attr('disabled', true);
            let ths = $(this)
            $('.spn').removeClass('d-none');
            let filetosend = $('input[name=editWithFile]').prop('files')[0];
            let id= $(this).data('id')
            console.log($('input[name=editWithFile]').prop('files')[0]);
            let formdata = new FormData();
            formdata.append('file', filetosend);
            formdata.append('id', id);
            $.ajax({
                url: "{{route('uploadDocumentImage')}}",
                type: "POST",
                data: formdata,
                contentType: false,
                processData: false,
                success: function(data) {
                    console.log(data);
                    $('#doEdit').attr('disabled', false);
                    $('.spn').addClass('d-none');
                    if (data.success) {
                        $('#docContainer').empty()
                        $('#docContainer').append("<a href='' onclick='return false'>"+data.name+"</a>  <b class='pull-right removeDoc' data-id='"+data.id+"' data-name='"+data.name+"' style='cursor: pointer; color: red'><i class='fa fa-times'></i>Remove</b><div class='spinner-border spn d-none'></div>")
                    }else{
                        $('#errors').modal('toggle');
                        let msg = "<p>There was an error laoding the file.</p>";
                        $('#message').html(msg);
                    }
                }
            });
        });
    </script>
@endsection('script')