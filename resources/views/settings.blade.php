@extends('Layouts.dashboardLayout')
@section('content')
    <div class="slim-mainpanel">
        <div class="container">
            <div class="slim-pageheader">
                <ol class="breadcrumb slim-breadcrumb">
                    <li class="breadcrumb-item"><a href="{{route('dashboard')}}">Dashbaord</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Settings</li>
                </ol>
                <h6 class="slim-pagetitle"></h6>
            </div><!-- slim-pageheader -->
            @include('includes.messages')
            <div class="section-wrapper">
                <label class="section-title">Change your info</label>
                <p class="mg-b-20 mg-sm-b-40"> </p>
                <div class="col-md-6">
                    <div class="box bg-white col-md-12">
                        <p><h4> <small>Personal</small></h4></p>
                        <form action="{{route('admin.updateMe')}}" method="post">
                            {{csrf_field()}}
                            <div class="card-body">
                                <div class="form-group">
                                    <label>Email</label>
                                    <input type="text" class="form-control" name="email" value="{{Auth::user()->email}}">
                                </div>
                                <div class="form-group">
                                    <label>Firstname</label>
                                    <input type="text" class="form-control" name="firstname" value="{{Auth::user()->firstname}}">
                                </div>
                                <div class="form-group">
                                    <label>Lastname</label>
                                    <input type="text" class="form-control" name="lastname" value="{{Auth::user()->lastname}}">
                                </div>
                                {{--<div class="form-group">
                                    <label>Email</label>
                                    <input type="text" class="form-control">
                                </div>--}}
                                <div class="form-group">
                                    <div><button class="btn btn-success pull-right mt-2 mb-2" type="submit">Update</button></div>
                                </div>
                            </div>
                        </form>

                    </div>
                </div>


                <div class="col-md-6">
                    <div class="box bg-white col-md-12">
                        <p><h4> <small>Change Password</small></h4></p>
                        <div class="card-body">
                            <form action="{{route('admin.changePassword')}}" method="post">
                                {{csrf_field()}}
                                <div class="form-group">
                                    <label>Old Password</label>
                                    <input type="text" class="form-control" name="oldpassword">
                                </div>

                                <div class="form-group">
                                    <label>New Password</label>
                                    <input type="text" class="form-control" name="newpassword">
                                </div>

                                <div class="form-group">
                                    <label>Confirm New Password</label>
                                    <input type="text" class="form-control" name="confirmnewpassword">
                                </div>
                                <div class="form-group">
                                    <div><button class="btn btn-success pull-right mt-2 mb-2">Update Password</button></div>
                                </div>
                            </form>

                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
    {{--Create task modal--}}
    <div id="createTask" class="modal fade" aria-hidden="true" style="">
        <div class="modal-dialog modal-dialog-vertical-center" role="document">
            <div class="modal-content bd-0 tx-14">
                <div class="modal-header pd-y-20 pd-x-25">
                    <h6 class="tx-14 mg-b-0 tx-uppercase tx-inverse tx-bold">Add Task</h6>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body pd-25">
                    <h5 class="lh-3 mg-b-20"><a href="#" class="tx-inverse hover-primary"> Fill in the details to add task <div id="taskName"></div></a></h5>
                    <div class="form-group">
                        <label>Task Name</label>
                        <input class="form-control" type="text" name="taskname" placeholder="Task Name">
                    </div>
                    <div class="form-group">
                        <label>Description</label>
                        <textarea class="form-control" id="description"></textarea>
                    </div>
                    <div class="form-group">
                        <label>Expected Start Date</label>
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <div class="input-group-text">
                                    <i class="fa fa-calendar tx-16 lh-0 op-6"></i>
                                </div>
                            </div>
                            <input type="text" name="sdate" id="sdate" class="form-control fc-datepicker" data-provide='datepicker' data-date-container='#addTask' placeholder="YYYY-MM-DD">
                        </div>
                    </div>
                    <div class="form-group">
                        <label>Expected End Date</label>
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <div class="input-group-text">
                                    <i class="fa fa-calendar tx-16 lh-0 op-6"></i>
                                </div>
                            </div>
                            <input type="text" name="edate" class="form-control fc-datepicker" placeholder="YYYY-MM-DD" id="edate">
                        </div>
                    </div>
                    <div class="form-group">
                        <label>Quotation Prepared and Sent to client?</label>
                        <select class="form-control" name="quotationSent">
                            <option selected disabled>Select an Option</option>
                            <option value="1">Yes</option>
                            <option value="0">No</option>
                        </select>
                    </div>

                    <div id="dependentDiv" class="doNotShow">

                        <div class="form-group">
                            <label>Client's Name</label>
                            <div class="row"><div class="col-md-10"><input class="form-control" name="cltname" type="name"></div><div class="col-md-2"></div></div>
                        </div>
                        <div class="form-group">
                            <label>Client's email address</label>
                            <div class="row"><div class="col-md-10"><input class="form-control" name="cltemail" type="email"></div><div class="col-md-2"><i class="fa fa-circle indicator"></i></div></div>
                        </div>
                        <div class="form-group">
                            <label>Quotation Approved?</label>
                            <select class="form-control" name="approval">
                                <option selected disabled>Select an Option</option>
                                <option value="1">Yes</option>
                                <option value="0">No</option>
                            </select>
                        </div>
                        <div class="form-group doNotShow" id="tempHidden">
                            <label>Estimated Amount on Quatation</label>
                            <input type="text" name="amt" class="form-control">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" id="addTask" class="btn btn-primary" disabled>Add Task</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div><!-- modal-dialog -->
    </div>
    {{--create task modal end--}}
    @include('includes.alerts')
@endsection
@section('script')
    <script>
        $.ajaxSetup({
            headers:{'X-CSRF-TOKEN': $('meta[name=csrf-token]').attr('content')}
        });

        $('#sdate').datepicker({
            showOtherMonths: true,
            selectOtherMonths: true,
            dateFormat: 'yy-mm-dd'
        });

        $('#edate').datepicker({
            showOtherMonths: true,
            selectOtherMonths: true,
            dateFormat: 'yy-mm-dd'
        });

        $('input[name=amt]').bind('keyup paste', function(){
            this.value = this.value.replace(/[^0-9]/g, '');
            if(this.value !== "" && $(this).val() !== null){
                $('#addTask').attr('disabled', false);
            }else{
                $('#addTask').attr('disabled', true);
            }
        });

        function validateEmail(email) {
            var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
            if(emailReg.test( email )){
                return true;
            }else{
                return false;
            }
        }






    </script>
@endsection('script')