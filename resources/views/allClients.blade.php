@extends('Layouts.dashboardLayout')
@section('content')
    <div class="slim-mainpanel">
        <div class="container">
            <div class="slim-pageheader">
                <ol class="breadcrumb slim-breadcrumb">
                    <li class="breadcrumb-item"><a href="{{route('dashboard')}}">Dashbaord</a></li>
                    <li class="breadcrumb-item active" aria-current="page">All Clients</li>
                </ol>
                <h6 class="slim-pagetitle">All Clients</h6>
            </div><!-- slim-pageheader -->

            <div class="section-wrapper">
                <label class="section-title">List of all clients in the system</label>
                {{--<p class="mg-b-20 mg-sm-b-40">Projects Have tasks contained in them.  The tasks are tracked </p>--}}

                <div class="table-wrapper">
                    <table id="datatable1" class="table display table-responsive">
                        <!-- no-wrap  -->
                        <thead>
                        <tr>
                            <th class="">Client name</th>
                            <!-- wd-15p-force -->
                            <th class="">Assigned Staff</th>
                            <!-- wd-10p-force -->
                            <th class="">Client Email</th>
                            <!-- wd-10p-force -->
                            <th>Action</th>
                            <!-- "wd-30p-force" -->
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($clients as $client)
                            <tr>
                                <td>{{$client->name}}</td>
                                <td>{{$client->assigned_staff}}</td>
                                <td>{{$client->client_email}}</td>
                                <td><button class="btn btn-sm btn-info edt" data-id="{{$client->id}}" data-name="{{$client->name}}" data-clientmail="{{$client->client_email}}" data-staff="{{$client->assigned_staff}}"> Edit <i class="fa fa-edit"></i></button> </td>
                            </tr>
                        @endforeach
                        {{-- <buttton class="btn btn-info btn-sm">Mark as Completed</buttton>--}}
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    {{--Create task modal--}}
    <div id="createTask" class="modal fade" aria-hidden="true" style="">
        <div class="modal-dialog modal-dialog-vertical-center" role="document">
            <div class="modal-content bd-0 tx-14">
                <div class="modal-header pd-y-20 pd-x-25">
                    <h6 class="tx-14 mg-b-0 tx-uppercase tx-inverse tx-bold">Add Task</h6>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body pd-25">
                    <h5 class="lh-3 mg-b-20"><a href="#" class="tx-inverse hover-primary"> Fill in the details to add task <div id="taskName"></div></a></h5>
                    <div class="form-group">
                        <label>Task Name</label>
                        <input class="form-control" type="text" name="taskname" placeholder="Task Name">
                    </div>
                    <div class="form-group">
                        <label>Description</label>
                        <textarea class="form-control" id="description"></textarea>
                    </div>
                    <div class="form-group">
                        <label>Expected Start Date</label>
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <div class="input-group-text">
                                    <i class="fa fa-calendar tx-16 lh-0 op-6"></i>
                                </div>
                            </div>
                            <input type="text" name="sdate" id="sdate" class="form-control fc-datepicker" data-provide='datepicker' data-date-container='#addTask' placeholder="YYYY-MM-DD">
                        </div>
                    </div>
                    <div class="form-group">
                        <label>Expected End Date</label>
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <div class="input-group-text">
                                    <i class="fa fa-calendar tx-16 lh-0 op-6"></i>
                                </div>
                            </div>
                            <input type="text" name="edate" class="form-control fc-datepicker" placeholder="YYYY-MM-DD" id="edate">
                        </div>
                    </div>
                    <div class="form-group">
                        <label>Quotation Prepared and Sent to client?</label>
                        <select class="form-control" name="quotationSent">
                            <option selected disabled>Select an Option</option>
                            <option value="1">Yes</option>
                            <option value="0">No</option>
                        </select>
                    </div>

                    <div id="dependentDiv" class="doNotShow">

                        <div class="form-group">
                            <label>Client's Name</label>
                            <div class="row"><div class="col-md-10"><input class="form-control" name="cltname" type="name"></div><div class="col-md-2"></div></div>
                        </div>
                        <div class="form-group">
                            <label>Client's email address</label>
                            <div class="row"><div class="col-md-10"><input class="form-control" name="cltemail" type="email"></div><div class="col-md-2"><i class="fa fa-circle indicator"></i></div></div>
                        </div>
                        <div class="form-group">
                            <label>Quotation Approved?</label>
                            <select class="form-control" name="approval">
                                <option selected disabled>Select an Option</option>
                                <option value="1">Yes</option>
                                <option value="0">No</option>
                            </select>
                        </div>
                        <div class="form-group doNotShow" id="tempHidden">
                            <label>Estimated Amount on Quatation</label>
                            <input type="text" name="amt" class="form-control">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" id="addTask" class="btn btn-primary" disabled>Add Task</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div><!-- modal-dialog -->
    </div>
    {{--create task modal end--}}

    {{--edit prloject--}}
    <div id="editClient" class="modal fade" aria-hidden="true">
        <div class="modal-dialog modal-dialog-vertical-center" role="document">
            <div class="modal-content bd-0 tx-14">
                <div class="modal-header">
                    <h6 class="tx-14 mg-b-0 tx-uppercase tx-inverse tx-bold">Add a new Client</h6>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body pd-25">
                    <h5 class="lh-3 mg-b-20"><a href="#" class="tx-inverse hover-primary"> Fill in the details to create a new client.</a></h5>
                    <a href="/allClients">View all clients</a>
                    <input type="hidden" name="idntfy">
                    <div class="form-group">
                        <label>Client Name</label>
                        <input class="form-control" type="text" name="clientname" placeholder="Client Name">
                    </div>
                    <div class="form-group">
                        <label>XL Staff Email Assigned to Client</label>
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <div class="input-group-text">
                                    <i class="fa fa-mail-bulk tx-16 lh-0 op-6"></i>
                                </div>
                            </div>
                            <input type="text" name="smail" class="form-control" placeholder="XL Staff Email">
                        </div>
                    </div>
                    <div class="form-group">
                        <label>Contact Email of Client</label>
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <div class="input-group-text">
                                    <i class="fa fa-calendar tx-16 lh-0 op-6"></i>
                                </div>
                            </div>
                            <input type="text" name="clmail" class="form-control" placeholder="Client Email">
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" id="createClient" class="btn btn-primary">Edit Client</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div><!-- modal-dialog -->
    </div>
    {{--edit project--}}
    @include('includes.alerts')
@endsection
@section('script')
    <script>
        $.ajaxSetup({
            headers:{'X-CSRF-TOKEN': $('meta[name=csrf-token]').attr('content')}
        });

        $('#sdate').datepicker({
            showOtherMonths: true,
            selectOtherMonths: true,
            dateFormat: 'yy-mm-dd',
            changeMonth: true,
            changeYear: true
        });

        $('#edate').datepicker({
            showOtherMonths: true,
            selectOtherMonths: true,
            dateFormat: 'yy-mm-dd',
            changeMonth: true,
            changeYear: true
        });

        $('#pstartdate').datepicker({
            showOtherMonths: true,
            selectOtherMonths: true,
            dateFormat: 'yy-mm-dd',
            changeMonth: true,
            changeYear: true
        });

        $('#penddate').datepicker({
            showOtherMonths: true,
            selectOtherMonths: true,
            dateFormat: 'yy-mm-dd',
            changeMonth: true,
            changeYear: true
        });

        $('input[name=amt]').bind('keyup paste', function(){
            this.value = this.value.replace(/[^0-9]/g, '');
            if(this.value !== "" && $(this).val() !== null){
                $('#addTask').attr('disabled', false);
            }else{
                $('#addTask').attr('disabled', true);
            }
        });

        function validateEmail(email) {
            var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
            if(emailReg.test( email )){
                return true;
            }else{
                return false;
            }
        }

        $('input[name=cltemail]').bind('keyup blur', function () {
            validateEmail(this.value) ? $('.indicator').addClass('correct') : $('.indicator').addClass('error');
        });


        $('#datatable1').DataTable({
            //responsive: true,
            language: {
                searchPlaceholder: 'Search...',
                sSearch: '',
                lengthMenu: '_MENU_ items/page',
            }
        });

        $('select[name=quotationSent]').on('change', function () {
            if($('select[name=quotationSent] option:selected').text() == "Yes" ){
                $('#dependentDiv').removeClass('doNotShow');
            }else{
                $('#dependentDiv').addClass('doNotShow');
            }
        });

        $('select[name=approval]').on('change', function () {
            if($('select[name=approval] option:selected').text() == "Yes" ){
                $('#tempHidden').removeClass('doNotShow');
            }else{
                $('#tempHidden').addClass('doNotShow');
            }
        });

        let projectId = "";
        $('.launch').on('click',function () {
            projectId = $(this).data('id');
        });

        $(document).on('click', '#addTask', function(){
            let thisbutton = $(this);
            thisbutton.attr('disabled', true);
            let tname = $('input[name=taskname]').val();
            alert(projectId);
            let startdate = $('input[name=sdate]').val();
            let enddate = $('input[name=edate]').val();
            let qsent = $('select[name=quotationSent] option:selected').val();
            let approved = $('select[name=approval] option:selected').val();
            let amt = $('input[name=amt]').val();
            let cltemail = $('input[name=cltemail]').val();
            let cltname = $('input[name=cltname]').val();
            let description = $('#description').val();

            if(tname == null || tname == "" || startdate == "" || startdate == null || enddate == "" || enddate == null || cltemail == "" || cltemail == null){
                $('#error').modal('toggle');
                let msg = "<p>Please Provide a name for your task.</p><p>Start Date cannot be empty.</p><p>End date cannot be empty.</p><p>Client's email is compulsory.</p>";
                $('#message').html(msg);
                thisbutton.attr('disabled', false);
            }else{
                let postParameter = {taskname:tname, projectid:projectId, sdate:startdate, description:description, edate:enddate, quotationSent: qsent, quotationApproved:approved, cltname:cltname, cltemail:cltemail, amt:amt}
                $.post("{{route('addTask')}}", postParameter, function(data){
                    console.log(data);
                    thisbutton.attr('disabled', false);
                    if(data.task){
                        $('#createTask').modal('toggle');
                        $('input').each(function () {
                            $(this).val('');
                        });
                        $('#success').modal('toggle');
                        let msg = "<p>You successfully created the task named "+ data.task.task_name+" <a class='btn btn-sm btn-info' href='/tasks/"+projectId+"'>View</a></p>";
                        $('#successmsg').html(msg);
                    }else{
                        $('#error').modal('toggle');
                        let msg = "<p>There was an errors creating the task. Please try again.</p>";
                        $('#message').html(msg);
                    }
                });
            }
        });

        $(document).on('click', '.editp', function () {
            $('input[name=projectname]').val($(this).data('name'));
            $('input[name=pstartdate]').val($(this).data('psdate'));
            $('input[name=penddate]').val($(this).data('pfdate'));
            $('input[name=theid]').val($(this).data('id'));
            $('#editProject').modal('toggle');

        });

        $('#doEdit').on('click', function(){
            const sendthis = {id:$('input[name=theid]').val(), name: $('input[name=projectname]').val(), startdate: $('input[name=pstartdate]').val(), enddate: $('input[name=penddate]').val()}
            $(this).attr('disabled', true);
            $.post("{{route('editproject')}}", sendthis, function (data) {
                $('#doEdit').attr('disabled', false);
                if(data = "success"){
                    $('#success').modal('toggle');
                    let msg = "<p><b>You successfully edited this project!</b></p>";
                    $('#successmsg').html(msg);
                }else if(data = "error"){
                    $('#error').modal('toggle');
                    let msg = "<p><b>Error occurred while trying to edit the project.</b></p>";
                    $('#message').html(msg);
                }
            });
        });


        $(document).on('click', '.edt', function () {
            let btn = $(this);
            let id = btn.data('id');
            let name = btn.data('name');
            let client = btn.data('clientmail');
            let staff = btn.data('staff');
            $('input[name=clientname]').val(name);
            $('input[name=smail]').val(staff);
            $('input[name=clmail]').val(client);
            $('input[name=idntfy]').val(id);
            $('#editClient').modal('toggle')
        });

        $(document).on('click', '#createClient', function(){
            let thisbutton = $(this);
            thisbutton.prop('disabled', true);
            let id = $('input[name=idntfy]').val();
            let clientname = $('input[name=clientname]').val();
            let smail = $('input[name=smail]').val();
            let clmail = $('input[name=clmail]').val();

            if(smail == null || smail == "" || clientname == null ){
                $('#errors').modal('toggle');
                let msg = "<p>Please Provide a name for your project.</p>";
                $('#message').html(msg);
                thisbutton.attr('disabled', false);
            }else{
                $.post("{{route('editClient')}}", {id:id, clientname:clientname, smail:smail, clmail:clmail}, function(data){
                    console.log(data);
                    thisbutton.attr('disabled', false);
                    if(data.success){
                        $('#addClient').modal('toggle');
                        $('input').each(function () {
                            $(this).val('');
                        });
                        $('#success').modal('toggle');
                        let msg = "<p>You successfully updated client named "+ data.success.name+" <a href='/allClients'>View</a></p>";
                        $('#successmsg').html(msg);
                    }else{
                        $('#errors').modal('toggle');
                        let msg = "<p>There was an errors creating the project. Please try again.</p>";
                        $('#message').html(msg);
                    }
                });
            }
        });
    </script>
@endsection('script')