<!DOCTYPE html>
<html lang="en">

<!-- Mirrored from themepixels.me/slim1.1/template/page-signin2.html by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 02 Jun 2019 13:04:28 GMT -->
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="csrf-token" content="{{csrf_token()}}">

    <!-- Twitter -->
    <meta name="twitter:site" content="@themepixels">
    <meta name="twitter:creator" content="@themepixels">
    <meta name="twitter:card" content="summary_large_image">
    <meta name="twitter:title" content="Slim">
    <meta name="twitter:description" content="FMX | Project Tracking">
    <meta name="twitter:image" content="../../slim/img/slim-social.html">

    <!-- Facebook -->
    <meta property="og:url" content="http://themepixels.me/slim">
    <meta property="og:title" content="XL Outsourcing">
    <meta property="og:description" content="FMX | Project Tracking">

    <meta property="og:image" content="../../slim/img/slim-social.html">
    <meta property="og:image:secure_url" content="../../slim/img/slim-social.html">
    <meta property="og:image:type" content="image/png">
    <meta property="og:image:width" content="1200">
    <meta property="og:image:height" content="600">

    <!-- Meta -->
    <meta name="description" content="Outsourced Staff, Personnel Management">
    <meta name="author" content="Oyetola">

    <title>FMX Integrated Services | Project Tracker</title>

    <!-- Vendor css -->
    <link href="{{asset('lib/font-awesome/css/font-awesome.css')}}" rel="stylesheet">
    <link href="{{asset('lib/Ionicons/css/ionicons.css')}}" rel="stylesheet">

    <!-- Slim CSS -->
    <link rel="stylesheet" href="{{asset('css/slim.css')}}">

</head>
<body>
<div class="signin-wrapper">

    <div class="signin-box">
        <h2 class="slim-logo"><a href="">FMX Projects<span>.</span></a></h2>
        <h2 class="signin-title-primary">Welcome</h2>
        <h3 class="signin-title-secondary">Sign in to continue.</h3>

        @include('includes.messages')

        <div class="form-group">
            <input type="text" name="email" class="form-control" placeholder="Enter your email">
        </div><!-- form-group -->
        <div class="form-group mg-b-50">
            <input type="password" name="password" class="form-control" placeholder="Enter your password">
        </div><!-- form-group -->
        <div><div class="pull-left">Forgot Password? <a href="{{route('renderPasswordRecovery')}}">Recover it</a></div>  <div class="pull-right"><input type="checkbox" id="rem" name="remeber"><label for="rem">Remember me</label></div></div>
        <button class="btn btn-primary btn-block btn-signin" id="signIn">Sign In</button>
        {{--<p class="mg-b-0">Don't have an account? <a href="page-signup.html">Sign Up</a></p>--}}
    </div><!-- signin-box -->

</div><!-- signin-wrapper -->

@include('includes.alerts')
<script src=></script>
<script src="{{asset('lib/jquery/js/jquery.js')}}"></script>
<script src="{{asset('lib/bootstrap/js/bootstrap.js')}}"></script>
<script src="{{asset('js/slim.js')}}"></script>


<script>
    $.ajaxSetup({
        headers:{'X-CSRF-TOKEN': $('meta[name=csrf-token]').attr('content')}
    });

    $(document).on('click', '#signIn', function () {
        let ths =$(this);
        let email = $('input[name=email]').val();
        let password = $('input[name=password]').val();
        let checkbox = $('input[name=remember]');
        let remember;

        if(checkbox.prop('checked') == true){
            remember = true;
        }else{
            remember = false
        }
        $(this).attr('disabled', true);
        $(this).text('Please wait...');
        $.post('{{route("logUserIn")}}', {email:email, password:password, remember:remember}, function(data) {
            ths.attr('disabled', false);
            ths.text('Sign In');
            console.log(data);
            if(data.success){
                $('#success').modal('toggle');
                let msg = "<p>You are being redirected ...</p>";
                $('#successmsg').html(msg);
                location.href = "/dashboard";
            }else if(data.errors && data.errors == "Unauthorised"){
                $('#error').modal('toggle');
                let msg = "<p>Wrong Credentials. Please check your username and password and try again.</p>";
                $('#message').html(msg);
            }else if(data.suspended){
                $('#error').modal('toggle');
                let msg = "<p><b>You cannot login. You have been suspended</b></p>";
                {{--/let route = "{{route('confirmAccount', ['token'=> + data.token + ])}}"--}}
                $('#message').html(msg);
            }else{
                $('#error').modal('toggle');
                let msg = "<p>Ooops! Something ust have gone wrong. Please try again after 2 minutes.</p>";
                $('#message').html(msg);
            }

        });
    });

    $('input[name=password]').on('keypress', function(event) {
        var keycode = (event.keyCode ? event.keyCode : event.which);
        if (keycode == '13') {
            let ths =$('#signIn');
            let email = $('input[name=email]').val();
            let password = $('input[name=password]').val();
            $('#signIn').attr('disabled', true);
            $('#signIn').text('Please wait...');
            $.post('{{route("logUserIn")}}', {email:email, password:password}, function(data) {
                ths.attr('disabled', false);
                ths.text('Sign In');
                console.log(data);
                if(data.success){
                    $('#success').modal('toggle');
                    let msg = "<p>You are being redirected ...</p>";
                    $('#successmsg').html(msg);
                    location.href = "/dashboard";
                }else if(data.errors && data.errors == "Unauthorised"){
                    $('#error').modal('toggle');
                    let msg = "<p>Wrong Credentials. Please check your username and password and try again.</p>";
                    $('#message').html(msg);
                }else if(data.suspended){
                    $('#error').modal('toggle');
                    let msg = "<p><b>You cannot login. You have been suspended</b></p>";
                    {{--/let route = "{{route('confirmAccount', ['token'=> + data.token + ])}}"--}}
                    $('#message').html(msg);
                }else{
                    $('#error').modal('toggle');
                    let msg = "<p>Ooops! Something ust have gone wrong. Please try again after 2 minutes.</p>";
                    $('#message').html(msg);
                }

            });
        }
    });
</script>
</body>

<!-- Mirrored from themepixels.me/slim1.1/template/page-signin2.html by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 02 Jun 2019 13:04:28 GMT -->
</html>


