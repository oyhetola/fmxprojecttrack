<!DOCTYPE html>
<html lang="en">

<!-- Mirrored from themepixels.me/slim1.1/template/page-signin2.html by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 02 Jun 2019 13:04:28 GMT -->
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="csrf-token" content="{{csrf_token()}}">

    <!-- Twitter -->
    <meta name="twitter:site" content="@themepixels">
    <meta name="twitter:creator" content="@themepixels">
    <meta name="twitter:card" content="summary_large_image">
    <meta name="twitter:title" content="Slim">
    <meta name="twitter:description" content="FMX | Project Tracking">
    <meta name="twitter:image" content="">

    <!-- Facebook -->
    <meta property="og:url" content="">
    <meta property="og:title" content="XL Outsourcing">
    <meta property="og:description" content="FMX | Project Tracking">

    <meta property="og:image" content="">
    <meta property="og:image:secure_url" content="">
    <meta property="og:image:type" content="image/png">
    <meta property="og:image:width" content="1200">
    <meta property="og:image:height" content="600">

    <!-- Meta -->
    <meta name="description" content="Outsourced Staff, Personnel Management">
    <meta name="author" content="Oyetola">

    <title>FMX Integrated Services | Project Tracker</title>

    <!-- Vendor css -->
    <link href="{{asset('lib/font-awesome/css/font-awesome.css')}}" rel="stylesheet">
    <link href="{{asset('lib/Ionicons/css/ionicons.css')}}" rel="stylesheet">

    <!-- Slim CSS -->
    <link rel="stylesheet" href="{{asset('css/slim.css')}}">
    <link rel="stylesheet" href="{{asset('css/time-picker.css')}}">

</head>
<body class="dashboard-4">
<div class="slim-header">
    <div class="container">
        <div class="slim-header-left">
            <h2 class="slim-logo"><a href="">FMX Projects<span></span></a></h2>

            {{--<div class="search-box">
                <input type="text" class="form-control" placeholder="Search">
                <button class="btn btn-primary"><i class="fa fa-search"></i></button>
            </div><!-- search-box -->--}}
        </div><!-- slim-header-left -->
        <div class="slim-header-right">
        {{--<div class="dropdown dropdown-a">
            <a href="#" class="header-notification" data-toggle="dropdown">
                <i class="icon ion-ios-bolt-outline"></i>
            </a>
            <div class="dropdown-menu">
                <div class="dropdown-menu-header">
                    <h6 class="dropdown-menu-title">Activity Logs</h6>
                    <div>
                        <a href="#">Filter List</a>
                        <a href="#">Settings</a>
                    </div>
                </div><!-- dropdown-menu-header -->
                <div class="dropdown-activity-list">
                    <div class="activity-label">Today, December 13, 2017</div>
                    <div class="activity-item">
                        <div class="row no-gutters">
                            <div class="col-2 tx-right">10:15am</div>
                            <div class="col-2 tx-center"><span class="square-10 bg-success"></span></div>
                            <div class="col-8">cloud storage</div>
                        </div><!-- row -->
                    </div><!-- activity-item -->
                    --}}{{--<div class="activity-label">Yesterday, December 12, 2017</div>--}}{{--

                </div><!-- dropdown-activity-list -->
                <div class="dropdown-list-footer">
                    <a href="page-activity.html"><i class="fa fa-angle-down"></i> Show All Activities</a>
                </div>
            </div><!-- dropdown-menu-right -->
        </div>--}}<!-- dropdown -->
        {{--<div class="dropdown dropdown-b">
            <a href="#" class="header-notification" data-toggle="dropdown">
                <i class="icon ion-ios-bell-outline"></i>
                <span class="indicator"></span>
            </a>
            <div class="dropdown-menu">
                <div class="dropdown-menu-header">
                    <h6 class="dropdown-menu-title">Notifications</h6>
                    <div>
                        <a href="#">Mark All as Read</a>
                        <a href="#">Settings</a>
                    </div>
                </div><!-- dropdown-menu-header -->
                <div class="dropdown-list">
                    <!-- loop starts here -->
                    <a href="#" class="dropdown-link">
                        <div class="media">
                            <img src="../img/img8.jpg" alt="">
                            <div class="media-body">
                                <p><strong>Suzzeth Bungaos</strong> tagged you and 18 others in a post.</p>
                                <span>October 03, 2017 8:45am</span>
                            </div>
                        </div><!-- media -->
                    </a>
                    <!-- loop ends here -->
                    <div class="dropdown-list-footer">
                        <a href="page-notifications.html"><i class="fa fa-angle-down"></i> Show All Notifications</a>
                    </div>
                </div><!-- dropdown-list -->
            </div><!-- dropdown-menu-right -->
        </div>--}}<!-- dropdown -->
            @if(Auth::check())
            <div class="dropdown dropdown-c">
                <a href="#" class="logged-user" data-toggle="dropdown">
                    <img src="{{asset('img/user.png')}}" alt="">
                    <span>@if(Auth::check()){{Auth::user()->firstname}}@endif</span>
                    <i class="fa fa-angle-down"></i>
                </a>
                @if(Auth::check())
                    <div class="dropdown-menu dropdown-menu-right">
                        <nav class="nav">
                            {{--<a href="page-profile.html" class="nav-link"><i class="icon ion-person"></i> View Profile</a>
                            <a href="page-edit-profile.html" class="nav-link"><i class="icon ion-compose"></i> Edit Profile</a>
    --}}                    <a href="{{route('settings')}}" class="nav-link"><i class="icon ion-ios-gear"></i> Account Settings</a>
                            <a href="{{route('logUserOut')}}" class="nav-link"><i class="icon ion-forward"></i> Sign Out</a>
                        </nav>
                    </div><!-- dropdown-menu -->
                @endif
            </div><!-- dropdown -->
            @endif
        </div><!-- header-right -->
    </div><!-- container -->

</div>

<div class="container mt-lg-5">

    <div class="col-md-8 offset-md-2">
        <div class="mt-2"><h1>Hello, Welcome here!</h1></div>
        <p>Do you have any complaints to lodge about your project(s)/facilities? You can use the fields below and we will get back to you.</p>
        <div class="card card-body mt-3">
            <div class="card-title">Company Information</div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label>Company*</label>
                        <input class="form-control" name="company">
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label>Originating Department*</label>
                        <input class="form-control" name="department">
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label>Complainant's Email*</label>
                        <input class="form-control" name="complainant_email">
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label>Contact Number*</label>
                        <input class="form-control" name="complainant_phone">
                    </div>
                </div>
            </div>
            <hr>
            <div class="card-title">Complaint Details</div>
            <div class="row">
                <div class="col-md-4">
                    <label>Complaint Title</label>
                    <input class="form-control" name="complaint_title">
                </div>
                <div class="col-md-4">
                    <label>Date of Incidence(if relevant)</label>
                    <input class="form-control" id="incidence_date" name="incidence_date">
                </div>
                <div class="col-md-4">
                    <label>Time </label>
                    <input class="form-control" id="incidence_time" name="incidence_time">
                </div>
                <div class="col-md-12">
                    <label>Location of Incidence* </label>
                    <input class="form-control" name="location">
                </div>
            </div>
            <div class="form-group">
                <label>Your Message*</label>
                <textarea class="form-control" name="message" rows="6"></textarea>
            </div>
            <hr>
            <div class="card-title">Witness Details (If applicable)</div>
            <div class="row">
                <div class="col-md-4">
                    <label>Witness Name</label>
                    <input class="form-control" name="witness_name">
                </div>
                <div class="col-md-4">
                    <label>Email</label>
                    <input class="form-control" name="witness_email">
                </div>
                <div class="col-md-4">
                    <label>Contact Number</label>
                    <input class="form-control" name="witness_phone">
                </div>
            </div>
            <br>
            <hr>
            <div class="card-title">Signature</div>
            <div class="row">
                <div class="col-md-4"></div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label>Complainant's Name*</label>
                        <p style="color: red"><small>This would serve as your signature</small></p>
                        <input class="form-control" name="name">
                    </div>
                </div>
                <div class="col-md-4"></div>

            </div>


            <div class="form-row">
                <div class="col-md-4"></div>
                <div class="col-md-4"></div>
                <div class="col-md-4"><button class="btn btn-primary float-right" style="float: right !important;" id="complain">Submit</button></div>

            </div>
        </div>
    </div>
</div>

<div class="slim-footer" style="">
    <div class="container">
        <p>Copyright {{date('Y')}} &copy; All Rights Reserved. FMX | Project Tracker</p>
    </div><!-- container -->
</div><!-- slim-footer -->

@include('includes.alerts')
<script src=></script>
<script src="{{asset('lib/jquery/js/jquery.js')}}"></script>
<script src="{{asset('lib/bootstrap/js/bootstrap.js')}}"></script>
<script src="{{asset('js/slim.js')}}"></script>
<script src="{{asset('lib/jquery-ui/js/jquery-ui.js')}}"></script>
<script src="{{asset('js/time-picker.js')}}"></script>


<script>
    $.ajaxSetup({
        headers:{'X-CSRF-TOKEN': $('meta[name=csrf-token]').attr('content')}
    });

    $('#incidence_date').datepicker({
        showOtherMonths: true,
        selectOtherMonths: true,
        dateFormat: 'yy-mm-dd'
    });

    $('#incidence_time').timepicker({
        // Options
        timeSeparator: ':',
        showLeadingZero: true,

    showMinutesLeadingZero: true,

    showPeriod: false,
        showPeriodLabels: true,
        periodSeparator: ' ',
        altField: '#alternate_input',
        defaultTime: '12:34',

    // trigger options
    showOn: 'focus',
    });

    $(document).on('click', '#complain', function () {
        let ths =$(this);
        let email = $('input[name=complainant_email]').val();
        let phone = $('input[name=complainant_phone]').val();
        let company = $('input[name=company]').val();
        let department = $('input[name=department]').val();
        let location = $('input[name=location]').val();
        let title = $('input[name=complaint_title]').val();
        let date = $('input[name=incidence_date]').val();
        let time = $('input[name=incidence_time]').val();
        let message = $('textarea[name=message]').val();
        let witness_name = $('input[name=witness_name]').val();
        let witness_email = $('input[name=witness_email]').val();
        let witness_phone = $('input[name=witness_phone]').val();
        let name = $('input[name=name]').val();

        let send = {email:email, phone:phone, company:company, department:department, location:location, title:title, date:date, time:time, message:message, witness_name:witness_name, witness_email:witness_email,
                    witness_phone:witness_phone, name:name}
        console.log(send)
        $(this).attr('disabled', true);
        $(this).text('Please wait...');
        if(email == "" || phone == "" || department == "" || company == "" || message == "" || name==""){
            $('#error').modal('toggle');
            let msg = "<b><ul><li>Complainant's Email is required</li><li>Company name is required.</li><li>Message field is required</li><li>Company contact number required.</li><li>Complainant's name is required</li></ul></b>";
            $('#message').html(msg);
            $(this).attr('disabled', false);
            $(this).text('Submit');
            return;
        }
        //return;
        $.post('{{route("submitComplaint")}}', send, function(data) {
            ths.attr('disabled', false);
            ths.text('Submit');
            console.log(data);
            if(data.success){
                $('input').val('');
                $('textarea').val('');
                $('#success').modal('toggle');
                let msg = "<p>You successfully submitted your complaint.</p>";
                $('#successmsg').html(msg);
                window.location.href = "{{route('complaintSubmitted')}}";
            }else if(data.errors || data.errors == "Unauthorised"){
                $('#error').modal('toggle');
                let msg = "<p>Please check you filled in the asterisked fields and try again.</p>";
                $('#message').html(msg);
            }else if(data.error){
                if(data.error == "Address in mailbox given [nscsn] does not comply with RFC 2822, 3.6.2."){
                    $('#error').modal('toggle');
                    let msg = "<p>Email is not in the right format.</p>";
                    $('#message').html(msg);
                }else{
                    $('#error').modal('toggle');
                    let msg = "<p>Please check you filled in the asterisked fields and try again.</p>";
                    $('#message').html(msg);
                }
            } if(data.validationError){
                $('#error').modal('toggle');
                let msg = "<b><ul><li>Complainant's Email is required</li><li>Company name is required.</li><li>Message field is required</li><li>Company contact number required.</li><li>Complainant's name is required</li></ul></b>";
                $('#message').html(msg);
            }else{
                $('#error').modal('toggle');
                let msg = "<p>Ooops! Something ust have gone wrong. Please try again.</p>";
                $('#message').html(msg);
            }

        });
    });

</script>
</body>

<!-- Mirrored from themepixels.me/slim1.1/template/page-signin2.html by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 02 Jun 2019 13:04:28 GMT -->
</html>


