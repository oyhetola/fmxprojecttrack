@extends('Layouts.pageResponseLayout')
@section('content')
    <div class="slim-mainpanel">
        <div class="container">
            {{--<div class="slim-pageheader">
                <ol class="breadcrumb slim-breadcrumb">
                    <li class="breadcrumb-item"><a href="{{route('dashboard')}}">Dashbaord</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Project Approved</li>
                </ol>
                <h6 class="slim-pagetitle"></h6>
            </div>--}}<!-- slim-pageheader -->
            @include('includes.messages')

            <div class="mt-2">
                <h1>Complaint Lodged</h1>
                <p class="font-weight-bold">Your complaint has been logged. Thank you.</p>
            </div>
            {{--<div class="section-wrapper">

            </div>--}}
        </div>
    </div>

    @include('includes.alerts')
@endsection
@section('script')
    <script>
        $.ajaxSetup({
            headers:{'X-CSRF-TOKEN': $('meta[name=csrf-token]').attr('content')}
        });

    </script>
@endsection('script')