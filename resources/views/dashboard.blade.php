@extends('Layouts.dashboardLayout')
@section('content')
    <div>@include('includes.messages')</div>
    <div class="slim-mainpanel">
        <div class="container pd-t-50">
            <div class="row">

                <div class="col-lg-6">
                    <h3 class="tx-inverse mg-b-15">Welcome, {{Auth::user()->firstname}}!</h3>
                    {{--<p class="mg-b-40">.</p>--}}
                    <div class="row row-sm">
                        <div class="col-sm-3">
                            <button data-target="#addClient" data-toggle="modal" class="btn bg-emerald text-white btn-block">Add New Client</button>
                        </div><!-- col-6 -->
                        <div class="col-sm-3">
                            <button data-target="#createProject" data-toggle="modal" class="btn btn-primary btn-block">Add New Project</button>
                        </div><!-- col-6 -->
                        <div class="col-sm-3">
                            <button data-target="#addVendor" data-toggle="modal" class="btn btn-info btn-block">Add Vendor</button>
                        </div><!-- col-6 -->
                        <div class="col-sm-3 mg-t-2 mg-sm-t-0">
                            <a href="{{route('allProjects')}}" data-toggle="" class="btn btn-success btn-block">All Projects</a>
                        </div><!-- col-6 -->
                    </div>
                    <p class="mg-b-10">.</p>
                    <h6 class="slim-card-title mg-b-15">Your Stats</h6>
                    <div class="row no-gutters">
                        <div class="col-sm-4">
                            <div class="card card-earning-summary">
                                <h6>Total Projects</h6>
                                <h1>{{count($projects)}}</h1>
                                <span>Total Projects</span>
                            </div><!-- card -->
                        </div><!-- col-6 -->
                        <div class="col-sm-4">
                            <div class="card card-earning-summary mg-sm-l--1 bd-t-0 bd-sm-t">
                                <h6>Total Tasks</h6>
                                <h1>{{count($tasks)}}</h1>
                                <span>Total Tasks</span>
                            </div><!-- card -->
                        </div><!-- col-6 -->
                        <div class="col-sm-4">
                            <div class="card card-earning-summary mg-sm-l--1 bd-t-0 bd-sm-t">
                                <h6>Paid Projects</h6>
                                <h1>0</h1>
                                <span>Paid Projects</span>
                            </div><!-- card -->
                        </div><!-- col-6 -->
                        <div class="col-sm-4">
                            <div class="card card-earning-summary mg-sm-l--1 bd-t-0 bd-sm-t">
                                <a href="{{route('unfinishedProjects')}}"><h6>Total Unfinished Project</h6>
                                <h1>{{count($incompleteProjects)}}</h1>
                                    <span>Total Unfinished Tasks</span></a>
                            </div><!-- card -->
                        </div><!-- col-6 -->
                        <div class="col-sm-4">
                            <div class="card card-earning-summary mg-sm-l--1 bd-t-0 bd-sm-t">
                                <a href="{{route('unfinishedTasks')}}"><h6>Total Unfinished Task</h6>
                                <h1>{{count($incompleteTasks)}}</h1>
                                    <span>Total Unfinished Task</span></a>
                            </div><!-- card -->
                        </div><!-- col-6 -->

                    </div><!-- row -->
                </div><!-- col-6 -->
                <div class="col-lg-6 mg-t-20 mg-sm-t-30 mg-lg-t-0">
                    <div class="card card-dash-headline">
                        <h6 class="slim-card-title"></h6>
                        <h6 class="visitor-operating-label">Recent tasks status</h6>
                        @foreach($tasks as $key=>$task)
                            @if($key <= 9)
                            <label class="mg-b-5">{{$task->task_name}} {{number_format((($task->taskCount->process_count - ($task->skipped != null ? count( $task->skipped) : 0))/($task->skipped != null ?  8 - count( $task->skipped) : 8))*100, '2')}}%</label>
                            <div class="progress mg-b-15">
                                <div class="progress-bar bg-warning progress-bar-xs wd-{{$task->count}}p" role="progressbar" aria-valuenow="{{$task->count}}" aria-valuemin="0" aria-valuemax="100"></div>
                            </div><!-- progress -->
                            @endif
                        @endforeach

                    </div><!-- card -->
                </div><!-- col-6 -->
            </div><!-- row -->


                {{--Create project modal--}}
                    <div id="createProject" class="modal fade" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-vertical-center" role="document">
                        <div class="modal-content bd-0 tx-14">
                            <div class="modal-header">
                                <h6 class="tx-14 mg-b-0 tx-uppercase tx-inverse tx-bold">Create Project</h6>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                </button>
                            </div>
                            <div class="modal-body pd-25">
                                <h5 class="lh-3 mg-b-20"><a href="#" class="tx-inverse hover-primary"> Fill in the details to create your project</a></h5>
                                <div class="form-group">
                                    <label>Project Name</label>
                                    <input class="form-control" type="text" name="projectname" placeholder="Project Name">
                                </div>
                                <div class="form-group">
                                    <label>Client</label>
                                    <select class="form-control" name="cltname">
                                        <option selected disabled>All</option>
                                        @foreach($clients as $client)
                                            <option data-id="{{$client->id}}">{{$client->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Amount Required</label>
                                    <input type="text" class="form-control" name="amount">
                                </div>
                                <div class="form-group">
                                    <label>Expected Start Date</label>
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <div class="input-group-text">
                                                <i class="fa fa-calendar tx-16 lh-0 op-6"></i>
                                            </div>
                                        </div>
                                        <input type="text" name="sdate" id="sdate" class="form-control fc-datepicker" placeholder="YYYY-MM-DD">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label>Expected End Date</label>
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <div class="input-group-text">
                                                <i class="fa fa-calendar tx-16 lh-0 op-6"></i>
                                            </div>
                                        </div>
                                        <input type="text" name="edate" class="form-control fc-datepicker" placeholder="YYYY-MM-DD" id="edate">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label>Any Project Document? - Upload here (Max 2MB)</label>
                                    <input type="file" name="projectDocument" accept=".pdf,.doc,.docx,.xlsx,.xls,.txt,.ppt,.pptx">
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" id="addProject" class="btn btn-primary">Add Project</button>
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            </div>
                        </div>
                    </div><!-- modal-dialog -->
                </div>
                {{--create project modal end--}}

            {{--create client modal--}}
                <div id="addClient" class="modal fade" aria-hidden="true">
                <div class="modal-dialog modal-dialog-vertical-center" role="document">
                    <div class="modal-content bd-0 tx-14">
                        <div class="modal-header">
                            <h6 class="tx-14 mg-b-0 tx-uppercase tx-inverse tx-bold">Add a new Client</h6>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                        </div>
                        <div class="modal-body pd-25">
                            <h5 class="lh-3 mg-b-20"><a href="#" class="tx-inverse hover-primary"> Fill in the details to create a new client.</a></h5>
                            <a href="/allClients">View all clients</a>
                            <div class="form-group">
                                <label>Client Name</label>
                                <input class="form-control" type="text" name="clientname" placeholder="Client Name">
                            </div>
                            <div class="form-group">
                                <label>XL Staff Email Assigned to Client</label>
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text">
                                            <i class="fa fa-mail-bulk tx-16 lh-0 op-6"></i>
                                        </div>
                                    </div>
                                    <input type="text" name="smail" class="form-control" placeholder="XL Staff Email">
                                </div>
                            </div>
                            <div class="form-group">
                                <label>Contact Email of Client</label>
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text">
                                            <i class="fa fa-calendar tx-16 lh-0 op-6"></i>
                                        </div>
                                    </div>
                                    <input type="text" name="clmail" class="form-control" placeholder="Client Email">
                                </div>
                            </div>

                        </div>
                        <div class="modal-footer">
                            <button type="button" id="createClient" class="btn btn-primary">Create Client</button>
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div><!-- modal-dialog -->
            </div>
            {{--end create client modal--}}


            {{--create vendor modal--}}
            <div id="addVendor" class="modal fade" aria-hidden="true">
                <div class="modal-dialog modal-dialog-vertical-center" role="document">
                    <div class="modal-content bd-0 tx-14">
                        <div class="modal-header">
                            <h6 class="tx-14 mg-b-0 tx-uppercase tx-inverse tx-bold">Add a new Vendor</h6>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                        </div>
                        <div class="modal-body pd-25">
                            <h5 class="lh-3 mg-b-20"><a href="#" class="tx-inverse hover-primary"> Fill in the details to create a new vendor.</a></h5>
                            <a href="/allVendors">View all vendors</a>
                            <div class="form-group">
                                <label>Vendor Name</label>
                                <input class="form-control" type="text" name="vendorname" placeholder="Vendor Name">
                            </div>
                            <div class="form-group">
                                <label>Location</label>
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text">
                                            <i class="fa fa-map tx-16 lh-0 op-6"></i>
                                        </div>
                                    </div>
                                    <input type="text" name="location" class="form-control" placeholder="Location">
                                </div>
                            </div>


                        </div>
                        <div class="modal-footer">
                            <button type="button" id="createVendor" class="btn btn-primary">Create Vendor</button>
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div><!-- modal-dialog -->
            </div>
            {{--end create vendor modal--}}
            </div><!-- container -->
    </div><!-- slim-mainpanel -->
    </div>
    @include('includes.alerts')
@endsection
@section('script')
    <script>
        $.ajaxSetup({
            headers:{'X-CSRF-TOKEN': $('meta[name=csrf-token]').attr('content')}
        });

        $('#sdate').datepicker({
            showOtherMonths: true,
            selectOtherMonths: true,
            dateFormat: 'yy-mm-dd'
        });

        $('#edate').datepicker({
            showOtherMonths: true,
            selectOtherMonths: true,
            dateFormat: 'yy-mm-dd'
        });

        $('input[name=amount]').bind('keyup paste', function(){
            this.value = this.value.replace(/[^0-9,.]/g, '');
            if(this.value !== "" && $(this).val() !== null){
                $('#addTask').attr('disabled', false);
            }else{
                $('#addTask').attr('disabled', true);
            }
        });


        $(document).on('change', 'input[name=projectDocument]', function(){
            let thefile = $(this).prop('files')[0];
            var FileSize = $(this).prop('files')[0].size / 1024 / 1024; // in MB
            if (FileSize > 2) {
                $('#errors').modal('toggle');
                let msg = "<p>File cannot be more than 2 MB.</p>";
                $('#message').html(msg);
                alert('File cannot be more than 2 MB.');
                $(this).val('');
            }
        });

        $(document).on('click', '#addProject', function(){
            let formdata = new FormData();


            let thisbutton = $(this);
            thisbutton.attr('disabled', true);
            let pname = $('input[name=projectname]').val();
//            alert(pname);
            let startdate = $('input[name=sdate]').val();
            let enddate = $('input[name=edate]').val();
            let client  = $('select[name=cltname]').val();
            let amount  = $('input[name=amount]').val();
            let document = $('input[name=projectDocument]').prop('files')[0];
            /*if(document == "undefined" || document == null){
                document = null;
            }*/
            let postData = {projectname:pname, amount:amount, client:client, startdate:startdate, enddate:enddate};
            formdata.append('amount', amount);
            formdata.append('client', client);
            formdata.append('projectname', pname);
            formdata.append('startdate', startdate);
            formdata.append('enddate', enddate);
            formdata.append('document', document);
            thisbutton.text('Please wait...');
            if(pname == null || pname == "" || client == null){
                $('#errors').modal('toggle');
                let msg = "<p>Please Provide a name for your project.</p><p>Select a valid client</p>";
                $('#message').html(msg);
                thisbutton.attr('disabled', false);
                thisbutton.text('Add Project');
            }else{
                $.ajax({
                    url: "{{route('addProject')}}",
                    type: "POST",
                    data: formdata,
                    contentType: false,
                    processData: false,
                    success: function(data, textStatus, xhr) {
                        console.log(data);
                        thisbutton.attr('disabled', false);
                        thisbutton.text('Add Project');
                        if (data.project) {
                            $('#createProject').modal('toggle');
                            $('input').each(function () {
                                $(this).val('');
                            });
                            $('#success').modal('toggle');
                            let msg = "<p>You successfully created the project named " + data.project.name + " <a href='/allProjects'>View</a></p>";
                            $('#successmsg').html(msg);
                        }else if(xhr.status == 235){
                            $('#success').modal('toggle');
                            let msg = "<p>You successfully created the project named " + data.project.name + "but Approval Mail was not sent <a href='/allProjects'>View</a></p>";
                            $('#successmsg').html(msg);
                        } else {
                            $('#error').modal('toggle');
                            let msg = "<p>There was an error while creating the project. Please try again.</p>";
                            $('#message').html(msg);
                        }
                    }
                });
            }
        });

        $(document).on('click', '#createClient', function(){
            let thisbutton = $(this);
            thisbutton.prop('disabled', true);
            let clientname = $('input[name=clientname]').val();
            let smail = $('input[name=smail]').val();
            let clmail = $('input[name=clmail]').val();

            if(smail == null || smail == "" || clientname == null ){
                $('#errors').modal('toggle');
                let msg = "<p>Please Provide a name for your project.</p>";
                $('#message').html(msg);
                $('#errors').modal('toggle');
                thisbutton.attr('disabled', false);
            }else{
                $.post("{{route('addClient')}}", {clientname:clientname, smail:smail, clmail:clmail}, function(data){
                    console.log(data);
                    thisbutton.attr('disabled', false);
                    if(data.success){
                        $('#addClient').modal('toggle');
                        $('input').each(function () {
                            $(this).val('');
                        });
                        $('#success').modal('toggle');
                        let msg = "<p>You successfully created client named "+ data.success.name+" <a href='/allClients'>View</a></p>";
                        $('#successmsg').html(msg);
                    }else{
                        $('#errors').modal('toggle');
                        let msg = "<p>There was an errors creating the project. Please try again.</p>";
                        $('#message').html(msg);
                    }
                });
            }
        });

        $(document).on('click', '#createVendor', function(){
            let thisbutton = $(this);
            thisbutton.prop('disabled', true);
            let vendorname = $('input[name=vendorname]').val();
            let location = $('input[name=location]').val();

            if(location == null || vendorname == null ){
                $('#errors').modal('toggle');
                let msg = "<p>Please Provide a name for the vendor.</p>";
                $('#message').html(msg);
                $('#errors').modal('toggle');
                thisbutton.attr('disabled', false);
            }else{
                $.post("{{route('addVendor')}}", {vendorname:vendorname, location:location}, function(data){
                    console.log(data);
                    thisbutton.attr('disabled', false);
                    if(data.success){
                        $('#addVendor').modal('toggle');
                        $('input').each(function () {
                            $(this).val('');
                        });
                        $('#success').modal('toggle');
                        let msg = "<p>You successfully created vendor named "+ data.success.name+" <a href='/allClients'>View</a></p>";
                        $('#successmsg').html(msg);
                    }else{
                        $('#errors').modal('toggle');
                        let msg = "<p>There was an errors creating the project. Please try again.</p>";
                        $('#message').html(msg);
                    }
                });
            }
        });
    </script>
@endsection