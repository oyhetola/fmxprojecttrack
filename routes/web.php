<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
//    return \Illuminate\Support\Carbon::today('Africa/Lagos')->subWeek()->toDateString();
    //return $processCount= \App\Models\processCount::where('process_count', '<', 8)->whereDate('updated_at', '<=', \Illuminate\Support\Carbon::today('Africa/Lagos')->subWeek()->toDateTimeString())->get();

    return view('login');
})->name('viewLogin');

Route::get('/logout', 'loginController@logOut')->name('logUserOut');
Route::get('/login', 'loginController@logOut')->name('login');
Route::get('/register', 'registerController@renderRegister')->name('renderRegister');
Route::post('/register', 'registerController@register')->name('registerUser');
Route::post('/login', 'loginController@logUserIn')->name('logUserIn');
Route::get('/confirmAccount/{token}', 'registerController@confirmAccount')->name('confirmAccount');
Route::get('/dashboard', 'dashboardController@index')->name('dashboard');
Route::post('updateTaskStage', 'dashboardController@updateTaskStage')->name('updateTaskStage');
Route::post('stepTaskBackwards', 'dashboardController@stepTaskBackwards')->name('stepTaskBackwards');
Route::post('updateCompletionDate', 'dashboardController@updateCompletionDate')->name('updateCompletionDate');
Route::post('endProject', 'dashboardController@endProject')->name('endProject');
Route::post('editTask', 'dashboardController@editTask')->name('editTask');
Route::post('editproject', 'dashboardController@editproject')->name('editproject');
Route::get('allUsers', 'loginController@allUsers')->name('allUsers');
Route::put('suspendUser', 'loginController@suspendUser')->name('suspendUser');
Route::put('deleteUser', 'loginController@deleteUser')->name('deleteUser');
Route::post('addUser', 'loginController@addUser')->name('addUser');
Route::get('updateTaskStatus', 'dashboardController@updateTaskStatus')->name('updateTaskStatus');
//Route::get('updateTaskStatus', 'dashboardController@updateTaskStatus')->name('updateTaskStatus');
Route::get('generateInvoice/{id}', 'dashboardController@generateInvoice')->name('generateInvoice');
Route::get('unfinishedTasks', 'dashboardController@unfinishedTasks')->name('unfinishedTasks');
Route::get('unfinishedProjects', 'dashboardController@unfinishedProjects')->name('unfinishedProjects');
Route::get('settings', 'dashboardController@settings')->name('settings');
Route::post('addClient', 'dashboardController@addClient')->name('addClient');
Route::post('editClient', 'dashboardController@editClient')->name('editClient');
Route::get('allClients', 'dashboardController@allClients')->name('allClients');
Route::post('findemail', 'dashboardController@findemail')->name('findemail');
Route::get('approveProject', 'dashboardController@approveProject')->name('approveProject');
Route::post('checkIfApproved', 'dashboardController@checkIfApproved')->name('checkIfApproved');
Route::get('requestApproval/{id}', 'dashboardController@requestApproval')->name('requestApproval');

Route::post('/updateMe', 'dashboardController@updateMe')->name('admin.updateMe');
Route::post('/changePassword', 'dashboardController@changePassword')->name('admin.changePassword');
Route::get('recoverPassword', 'registerController@renderPasswordRecoverPage')->name('renderPasswordRecovery');
Route::post('recoverPass', 'registerController@recoverPass')->name('recoverPass');
Route::get('createNewPassword/{id}', 'registerController@createNewPassword')->name('createNewPassword');
Route::post('changeThePassword', 'registerController@changePassword')->name('changeThePassword');

Route::post('addVendor', 'dashboardController@addVendor')->name('addVendor');
Route::get('allVendors', 'dashboardController@allVendors')->name('allVendors');
Route::post('editVendor', 'dashboardController@editVendor')->name('editVendor');
Route::post('skipNextStep', 'dashboardController@skipNextStep')->name('skipNextStep');

Route::middleware('auth:web')->group(function(){
    Route::post('/addProject', 'dashboardController@addProject')->name('addProject');
    Route::get('/allProjects', 'dashboardController@displayAllProjects')->name('allProjects');
    Route::post('/addTask', 'dashboardController@addTask')->name('addTask');
    Route::get('/tasks/{id}', 'dashboardController@allTasks')->name('allTasks');
});

Route::get('/approveTask', 'dashboardController@approveTask')->name('approveTask');

Route::get('/downloadfile', 'dashboardController@downloadfile')->name('downloadfile');
Route::post('/deletedocfile', 'dashboardController@deletedocfile')->name('deletedocfile');
Route::post('/uploadDocumentImage', 'dashboardController@uploadDocumentImage')->name('uploadDocumentImage');

Route::get('/complain', 'dashboardController@complain')->name('complain');
Route::post('/submitComplaint', 'dashboardController@submitComplaint')->name('submitComplaint');
Route::get('/fetchComplaints', 'dashboardController@fetchComplaints')->name('fetchComplaints');
Route::get('/complaint-logged', function (){return view('complaintlodged');})->name('complaintSubmitted');
Route::get('/complaint-details/{id}', 'dashboardController@complaintDetails')->name('complaintDetails');
Route::post('/toggleResolve', 'dashboardController@toggleResolve')->name('toggleResolve');
Route::post('/unResolve', 'dashboardController@unResolve')->name('unResolve');
Route::get('/satisfied', 'dashboardController@satisfied')->name('satisfied');
Route::get('/unsatisfied', 'dashboardController@unsatisfied')->name('unsatisfied');