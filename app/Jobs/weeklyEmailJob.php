<?php

namespace App\Jobs;

use App\Mail\projectStatusReminder;
use App\Models\processCount;
use App\Models\task;
use App\User;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Mail;

class weeklyEmailJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    Public $timeout = 720;
    public function handle()
    {
        $processCount= processCount::where('process_count', '<', 8)->whereDate('updated_at', '<=', Carbon::today('Africa/Lagos')->subWeek()->toDateTimeString())->get();
        $users= User::get();
        $userEmails = [];
        foreach($users as $user){
            array_push($userEmails, $user->email);
        }
        if($processCount){
            $when = Carbon::now()->addMinute(4);
            foreach ($processCount as $cont){
                if($cont->task->quotation_status ==  0){
                    $body = "The last stage detected for this task (".$cont->task->task_name.") is at null,  The first stage is to <b>indicate if quotation has been prepared ans sent to client.</b> Please hit the botton to move to the next stage. Ignore if you are not ready to move to the next stage. ";
                    $newstage = "quotation_status";
                    $sendtheMail= new projectStatusReminder($cont, $body, $newstage);
                    Mail::to($userEmails)->later($when, $sendtheMail);
                }elseif($cont->task->quotation_status ==  1 && $cont->task->client_approval == 0){
                    $body = "The last stage detected for this task (".$cont->task->task_name.") is at quotation sent,  The next stage is to <b>indicate client's approval.</b> Please hit the botton to move to the next stage. Ignore if you are not ready to move to the next stage. ";
                    $newstage = "client_approval";
                    $sendtheMail= new projectStatusReminder($cont, $body, $newstage);
                    Mail::to($userEmails)->later($when, $sendtheMail);
                }elseif($cont->task->client_approval ==  1 && $cont->task->head_office_informed == 0){
                    $body = "The last stage detected for this task (".$cont->task->task_name.") is at client approval,  The next stage is to <b>inform head office</b> Please hit the botton to move to the next stage. Ignore if you are not ready to move to the next stage. ";
                    $newstage = "head_office_informed";
                    $sendtheMail= new projectStatusReminder($cont, $body, $newstage);
                    Mail::to($userEmails)->later($when, $sendtheMail);
                }elseif($cont->task->head_office_informed == 1 && $cont->task->head_office_approval == 0){
                    $body = "The last stage detected for this task (".$cont->task->task_name.") is at the stage head office was informed,  The next stage is to <b>get head office to approve</b> Please hit the button to move to the next stage. Ignore if you are not ready to move to the next stage. ";
                    $newstage = "head_office_approval";
                    $sendtheMail= new projectStatusReminder($cont, $body, $newstage);
                    Mail::to($userEmails)->later($when, $sendtheMail);
                }elseif ($cont->task->head_office_approval == 1 && Carbon::now('Africa/Lagos')->toDateString() > $cont->task->expected_finish_date){
                    $body = "The last stage detected for this task (".$cont->task->task_name.") is at the stage head office approved,  The next stage is to <b>indicate if execution is complete.</b> Please hit the button to move to the next stage. Ignore if you are not ready to move to the next stage. ";
                    $newstage = "execution_status";
                    $sendtheMail= new projectStatusReminder($cont, $body, $newstage);
                    Mail::to($userEmails)->later($when, $sendtheMail);
                }elseif ($cont->task->execution_status ==1 &&  $cont->task->jcc_status == 0){
                    $body = "The last stage detected for this task (".$cont->task->task_name.") is at the stage execution of the project was confirmed,  The next stage is to <b> seek/collect JCC.</b> Please hit the button to confirm if JCC is granted. Ignore if you are not ready to move to the next stage. ";
                    $newstage = "jcc_status";
                    $sendtheMail= new projectStatusReminder($cont, $body, $newstage);
                    Mail::to($userEmails)->later($when, $sendtheMail);
                }elseif ($cont->task->jcc_status == 1 && $cont->task->invoice_status == 0 ){
                    $body = "The last stage detected for this task (".$cont->task->task_name.") is at the stage JCC was granted,  The next stage is to <b> generate ivoice.</b> Please hit the button to move to that stage. Ignore if you are not ready to move to the next stage. ";
                    $newstage = "invoice_status";
                    $sendtheMail= new projectStatusReminder($cont, $body, $newstage);
                    Mail::to($userEmails)->later($when, $sendtheMail);
                }elseif ($cont->task->invoice_status == 1 && $cont->task->payment_status == 0){
                    $body = "The last stage detected for this task (".$cont->task->task_name.") is at the stage Inovice as generated,  The next stage is to <b> ascertain if you received payment.</b> Please hit the button to confirm. Ignore if you are not ready to move to the next stage. ";
                    $newstage = "payment_status";
                    $sendtheMail= new projectStatusReminder($cont, $body, $newstage);
                    Mail::to($userEmails)->later($when, $sendtheMail);
                }
            }
        }

    }
}
