<?php

namespace App\Jobs;

use App\Mail\projectCompletionReminder;
use App\Models\project;
use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Mail;

class projectCompletionCheck implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $projects = project::where('completion_status', 0)->where('finish_date', '<=', Carbon::now('Africa/Lagos'))->get();
        $users = User::all();
        $emails = [];
        foreach ($users as $user){
            array_push($emails, $user->email);
        }
        Mail::to($emails)->later(Carbon::now('Africa/Lagos')->addMinute(3), new projectCompletionReminder($projects));
    }
}
