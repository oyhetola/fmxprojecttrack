<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class projectStatusReminder extends Mailable implements  ShouldQueue
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $cont;
    public $newStage;
    public $body;
    public function __construct($cont, $body, $newstage)
    {
        $this->body = $body;
        $this->newStage = $newstage;
        $this->cont = $cont;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('Emails.weeklyReminder');
    }
}
