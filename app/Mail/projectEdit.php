<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class projectEdit extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    Public $project;
    public $user;
    Public $previousName;
    public function __construct($project, $user, $previous)
    {
        $this->project = $project;
        $this->user = $user;
        $this->previousName = $previous;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('Emails.projectEdit')->subject('Project Edited');
    }
}
