<?php

namespace App\Console;

use App\Jobs\projectCompletionCheck;
use App\Jobs\weeklyEmailJob;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // $schedule->command('inspire')
        //          ->hourly();
        $schedule->job(new weeklyEmailJob)->weeklyOn(1, '11:00')->timezone('Africa/Lagos');
        $schedule->job(new projectCompletionCheck())->dailyAt('10:00')->timezone('Africa/Lagos');
        //$schedule->command('queue:work')->twiceDaily(12, 18)->withoutOverlapping()->timezone('Africa/Lagos');
        $schedule->command('queue:forget')->dailyAt('23:00')->timezone('Africa/Lagos');
//        $schedule->command('queue:listen --daemon')->everyTenMinutes();

    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
