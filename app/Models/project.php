<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class project extends Model
{
    protected $fillable = ['name', 'client', 'budget', 'start_date', 'finish_date', 'created_by', 'document', 'completion_status'];

    Public function user(){
        return $this->belongsTo('App\User', 'created_by', 'id');
    }

    Public function  approve(){
        return $this->belongsTo('App\User', 'approved_by', 'id');
    }
}
