<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class task extends Model
{
    protected $fillable = ['project_id', 'task_name', 'description', 'quotation_status', 'client_approval', 'head_office_informed', 'head_office_approval',
        'approval_date',  'client_name', 'client_email', 'invoice_status', 'payment_status', 'amount_due', 'amount_paid', 'jcc_status', 'expected_start_date', 'expected_finish_date',
        'actual_completion_date', 'created_by', 'updated_by', 'vendor', 'supervisor', 'project_type', ];

    protected $casts = ['skipped'=>'array'];

    public function project(){
        return $this->belongsTo('App\Models\project', 'project_id', 'id');
    }

    public function user(){
        return $this->belongsTo('App\User', 'created_by', 'id');
    }

    public function  taskCount(){
        return $this->hasOne('App\Models\processCount', 'task_id', 'id');
    }

    public function  updatingTask(){
        return $this->belongsTo('App\User', 'updated_by', 'id');
    }
}
