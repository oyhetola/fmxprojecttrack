<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class complaint extends Model
{
    protected $fillable = ['company', 'department', 'email', 'phone', 'title', 'date', 'time', 'location', 'detailed_complaint', 'witness_name', 'witness_email',
                           'witness_phone', 'complainants_name', 'resolution_status', 'client_satisfaction'];
}
