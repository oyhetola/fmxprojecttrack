<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class processCount extends Model
{
    protected $fillable = ['project_id', 'task_id', 'process_count', 'updated_by'];
    public function task(){
        return $this->belongsTo('App\Models\task', 'task_id', 'id');
    }

    public function project(){
        return $this->belongsTo('App\Models\project', 'project_id', 'id');
    }

    public function user(){
        return $this->belongsTo('App\User', 'updated_by', 'id');
    }
}
