<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class loginController extends Controller
{
    Public $successStatus = 200;

    function logUserIn(){
        try{
            if(Auth::attempt(['email' => request('email'), 'password' => request('password')])){
                $user = Auth::user();
              if($user->suspend != 1){
                  \request('remember') ? Auth::login($user, \request('remember')): '';
                  return response()->json(['success' => "logged in"], $this->successStatus);
              }else{
                  Auth::logout();
                  return response()->json(['suspended' => "suspended"], $this->successStatus);
              }
            }else{
                return response()->json(['errors'=>'Unauthorised', 'status'=> 401]);
                //  abort(401);
            }
        }catch(\Exception $e){
            return response()->json(['ooops'=>$e->getMessage(), 'status'=> 503]);
        }
    }

    function allUsers(){
        $users = User::where('delete_status', 0)->get();
        return view('users', ['users'=>$users, 'page'=>'users']);
    }

    function suspendUser(Request $request){
        try{
            $user = User::where('id', $request->id)->first();
            if($user->suspend == 0){
                $user->suspend = 1;
            }else{
                $user->suspend = 0;
            }
            $user->update();
            return response()->json(['success'=>'suspended'], 200);
        }catch (\Exception $e){
            return response()->json(['error'=>$e->getMessage()], 500);
        }
    }

    function deleteUser(Request $request){
        try{
            $user = User::where('id', $request->id)->first();
            $user->delete_status = 1;
            $user->update();
            return response()->json(['success'=>'deleted'], 200);
        }catch (\Exception $e){
            return response()->json(['error'=>$e->getMessage()], 500);
        }
    }

    function addUser(Request $request){
        try{
            $user = User::create([
                'firstname'=>$request->fname,
                'lastname' => $request->lname,
                'email' => $request->email,
                'password' => bcrypt('123456'),
                'role' => $request->role,
            ]);
            return response()->json(['success'=>$user], 200);
        }catch (\Exception $e){
            return response()->json(['error'=>$e->getMessage()], 500);
        }
    }

    function logOut(){
        Auth::logout();
        return redirect(route('viewLogin'));
    }
}
