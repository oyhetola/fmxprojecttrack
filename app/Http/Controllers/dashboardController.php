<?php

namespace App\Http\Controllers;

use App\Mail\approvalRequest;
use App\Mail\ApprovedProject;
use App\Mail\complaintNotification;
use App\Mail\complaintRessolutionUpdate;
use App\Mail\editTaskNotification;
use App\Mail\newProjectNotification;
use App\Mail\newTaskNotification;
use App\Mail\projectEdit;
use App\Mail\taskApproved;
use App\Mail\TaskUpdateNotification;
use App\Models\client;
use App\Models\complaint;
use App\Models\processCount;
use App\Models\project;
use App\Models\task;
use App\User;
use App\vendor;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class dashboardController extends Controller
{
    function index(){
        if(Auth::check()){
            //return Carbon::today('Africa/Lagos')->subWeek()->toDateTimeString();
//           return $processCount= processCount::where('process_count', '<', 8)->whereDate('updated_at', '<=', Carbon::today('Africa/Lagos')->subWeek()->toDateTimeString())->get();
          $tasks = task::latest()->get();
          $clients = client::all();
            $projects = project::all();
            foreach ($tasks as $task){
                $task['count'] = round(($task->taskCount->process_count/8)*100, 0, PHP_ROUND_HALF_UP);
            }
            $incompleteProjects = project::where('completion_status', 0)->get();
            $incompleteTasks = task::where('payment_status', 0)->get();
            //$paidTask = task::where('payment_status', 0)->whereDate()->get();
            //return $tasks;
            return view('dashboard')->with(['tasks'=>$tasks, 'clients'=>$clients, 'projects'=>$projects,'incompleteProjects'=>$incompleteProjects,'incompleteTasks'=>$incompleteTasks, 'page'=>'index']);
        }else{
            return redirect()->route('viewLogin');
        }
    }

    function addProject(Request $request){
        try{
            $filevalue = null;
//            return $request->document;
            if($request->hasFile('document')){
                $file = $request->file('document');
                $filevalue = $file->getClientOriginalName();
            }

            $project = project::create([
                'name' => $request->projectname,
                'client' => $request->client,
                'budget' => $request->amount,
                'start_date' => $request->startdate,
                'finish_date' => $request->enddate,
                'document' => $filevalue,
                'created_by' => Auth::id(),
            ]);

            if($project){
                if($request->hasFile('document') )
                    Storage::putFileAs('public/projectDocuments/'.$project->id, $file, $file->getClientOriginalName());
                $when = Carbon::now()->addMinute(3);
                $admin = User::where('role', 'Admin')->get();
                $users =User::where('role', 'Manager')->where('suspend', 0)->get();
                $emails = [];

                Mail::to($admin[0]->email)->send(new newProjectNotification($project, $admin[0]));
//            , $admin[0]->role
                if(sizeof($users) > 0){
                    foreach($users as $user){
                        //array_push($emails, $user->email);
                        Mail::to($user->email)->send(new newProjectNotification($project, $user));
//                , $user->role
                    }
                }

                return response()->json(['project'=>$project], 200);
            }else{
                return response()->json(['error'=>'error'], 500);
            }
        }catch (\Exception $e){
            return $e->getMessage();
        }
    }

    function  displayAllProjects(){
        $projects= project::latest()->get();
        $clients = client::all();
        $users = User::where('role', 'Officer')->orWhere('role', 'Manager')->get();
        $vendors = vendor::all();
        return view('allProjects', ['projects'=>$projects, 'clients'=>$clients, 'page'=>'projects', 'supervisors'=>$users, 'vendors'=>$vendors]);
    }

    function findemail(Request $request){
        $email = client::where('id', $request->id)->first();
        return $email->client_email;
    }

    function  addTask(Request $request){
        try{
//            return $request;
            $task = task::create([
                'project_id'=> $request->projectid,
                'task_name'=>$request->taskname,
                'description'=>$request->description,
                'quotation_status'=>$request->quotationSent,
                'client_approval'=>$request->quotationApproved,
                'amount_due'=> $request->amt,
                'client_name'=> $request->cltname,
                'client_email'=> $request->cltemail,
                'expected_start_date'=>$request->sdate,
                'expected_finish_date'=>$request->edate,
                'created_by'=>Auth::id(),
                'supervisor'=>$request->supervisor,
                'vendor'=>$request->vendor,
                'project_type'=>$request->projectType
            ]);

            if($task){
                $when = Carbon::now()->addMinute(3);
                $admin = User::where('role', 'Admin')->get();
                $users =User::where('role', 'Manager')->orWhere('role', 'Accountant')->get();
                $emails = [];
                foreach ($users as $user){
                    array_push($emails, $user->email);
                }
                $theCount = 0;
                if($request->quotationSent == 1 && $request->quotationApproved == 1){
                    $theCount = $theCount + 2;
                }elseif($request->quotationSent == 0 && $request->quotationApproved == 1){
                    $theCount = $theCount + 1;
                }elseif($request->quotationSent == 1 && $request->quotationApproved == 0){
                    $theCount = $theCount + 1;
                }else{
                    $theCount = 0;
                }
                Mail::to($admin[0]->email)->cc($emails)->send(new newTaskNotification($task));
                processCount::create(['project_id'=>$task->project_id, 'task_id'=>$task->id, 'process_count'=> $theCount, 'updated_by'=>Auth::id()]);
                return response()->json(['task'=>$task], 200);
            }else{
                return response()->json(['error'=>'error'], 500);
            }
        }catch (\Exception $e){
            return $e->getMessage();
        }

    }

    function allTasks($id){
        if(Auth::check()){
            try{
                $tasks = task::where('project_id', $id)->get();
                $clients = client::all();
                $supervisors = User::where('role', 'Manager')->orWhere('role', 'Officer')->get();
                $vendors = client::all();
                return view('tasks')->with(['tasks'=>$tasks, 'clients'=>$clients, 'supervisors'=>$supervisors, 'vendors'=>$vendors]);
            }catch (\Exception $e){
                return $e->getMessage();
            }
        }else{
            return redirect()->route('viewLogin');
        }

    }

    function updateTaskStage(Request $request){
        try{
            $holdit = "";
            $task = task::where('id', $request[0])->first();
            if($request[1] == "quotation_sent"){
                $task->quotation_status = 1;
                $holdit = $request[1];
            }elseif($request[1] == "quotation_approved"){
                $task->client_approval = 1;
                $holdit = $request[1];
            }
            elseif($request[1] == "head_office_informed"){
                $task->head_office_informed = 1;
                $holdit = $request[1];
            }elseif ($request[1] == "head_office_approval"){
                $task->head_office_approval = 1;
                $holdit = $request[1];
            }elseif ($request[1] == "execution_status"){
                $task->execution_status = 1;
                $holdit = $request[1];
            }elseif ($request[1] == "jcc_status"){
                $task->jcc_status = 1;
                $holdit = $request[1];
            }elseif ($request[1] == "invoice_status"){
                $task->invoice_status = 1;
                $holdit = $request[1];
            }elseif ($request[1] == "payment_status"){
                $task->payment_status = 1;
                $holdit = $request[1];
            }
            $untracked = 0;
            if($task->skipped != null){
                $temp = [];
                $temp = $task->skipped;
                if(in_array($request[1], $temp)){
                    $temp = array_diff($temp, [$request[1]]);
                    $task->skipped = $temp;
                    $untracked = count($temp);
                }
            }
            $task->update();
            $incrementProcessCount = processCount::where(['task_id'=>$task->id, 'project_id'=>$task->project_id])->first();
            if($incrementProcessCount){
                if($incrementProcessCount->process_count < 8){
                    if($request[1] == "quotation_sent"){
                        $incrementProcessCount->process_count = 1;
                    }elseif($request[1] == "quotation_approved"){
                        $incrementProcessCount->process_count = 2;
                    }
                    elseif($request[1] == "head_office_informed"){
                        $incrementProcessCount->process_count = 3;
                    }elseif ($request[1] == "head_office_approval"){
                        $incrementProcessCount->process_count = 4;
                    }elseif ($request[1] == "execution_status"){
                        $incrementProcessCount->process_count = 5;
                    }elseif ($request[1] == "jcc_status"){
                        $incrementProcessCount->process_count = 6;
                    }elseif ($request[1] == "invoice_status"){
                        $incrementProcessCount->process_count = 7;
                    }elseif ($request[1] == "payment_status"){
                        $incrementProcessCount->process_count = 8;
                    }
                    $incrementProcessCount->update();
                }
            }else{
                processCount::create([
                    'project_id' => $task->project_id,
                    'task_id' => $task->id,
                    'process_count' => 0+1,
                    'updated_by' => Auth::id()
                ]);
            }
            $when = Carbon::now()->addMinute(3);
            $admin = User::where('role', 'Admin')->get();
            $users =User::where('role', 'Manager')->get();
            $emails = [];
            foreach ($users as $user){
                array_push($emails, $user->email);
            }
            Mail::to($admin[0]->email)->cc($emails)->later($when, new taskUpdateNotification($task, $holdit));
            return response()->json(['task'=>$task]);
        }catch (\Exception $e){
            return $e->getMessage();
        }
    }

    function stepTaskBackwards(Request $request){
        try{
            $holdit = "";
            $previous = "";
            $task = task::where('id', $request[0])->first();
            if($request[1] == "quotation_sent"){
                $task->quotation_status = 0;
                $holdit = $request[1];
                $previous = "";
            }elseif($request[1] == "quotation_approved"){
                $task->client_approval = 0;
                $holdit = $request[1];
                $previous = "quotation_sent";
            }elseif($request[1] == "head_office_informed"){
                $task->head_office_informed = 0;
                $holdit = $request[1];
                $previous = "quotation_approved";
            }elseif ($request[1] == "head_office_approval"){
                $task->head_office_approval = 0;
                $holdit = $request[1];
                $previous = "head_office_informed";
            }elseif ($request[1] == "execution_status"){
                $task->execution_status = 0;
                $holdit = $request[1];
                $previous = "head_office_approval";
            }elseif ($request[1] == "jcc_status"){
                $task->jcc_status = 0;
                $holdit = $request[1];
                $previous = "execution_status";
            }elseif ($request[1] == "invoice_status"){
                $task->invoice_status = 0;
                $holdit = $request[1];
                $previous = "jcc_status";
            }elseif ($request[1] == "payment_status"){
                $task->payment_status = 0;
                $holdit = $request[1];
                $previous = "invoice_status";
            }
            $untracked = 0;
            if($task->skipped != null){
                $temp = [];
                $temp = $task->skipped;
                if(in_array($request[1], $temp)){
                    $temp = array_diff($temp, [$request[1]]);
                    $task->skipped = $temp;
                    $untracked = count($temp);
                }
                if(in_array($previous, $temp)){
                    $temp = array_diff($temp, [$previous]);
                    $task->skipped = $temp;
                    $untracked = count($temp);
                }

            }
            $task->update();
            $incrementProcessCount = processCount::where(['task_id'=>$task->id, 'project_id'=>$task->project_id])->first();
            if($incrementProcessCount){
                if($incrementProcessCount->process_count > 1){
                    if($incrementProcessCount->process_count == 8){
                        $incrementProcessCount->task->actual_completion_date = null;
                        $incrementProcessCount->task->save();
                    }
                    //$incrementProcessCount->process_count = $incrementProcessCount->process_count - 1;
                    if($request[1] == "quotation_sent"){
                        $incrementProcessCount->process_count = 0;
                    }elseif($request[1] == "quotation_approved"){
                        $incrementProcessCount->process_count = 1;
                    }
                    elseif($request[1] == "head_office_informed"){
                        $incrementProcessCount->process_count = 2;
                    }elseif ($request[1] == "head_office_approval"){
                        $incrementProcessCount->process_count = 3;
                    }elseif ($request[1] == "execution_status"){
                        $incrementProcessCount->process_count = 4;
                    }elseif ($request[1] == "jcc_status"){
                        $incrementProcessCount->process_count = 5;
                    }elseif ($request[1] == "invoice_status"){
                        $incrementProcessCount->process_count = 6;
                    }elseif ($request[1] == "payment_status"){
                        $incrementProcessCount->process_count = 7;
                    }
                    $incrementProcessCount->update();
                }
            }
            $when = Carbon::now()->addMinute(3);
            $admin = User::where('role', 'Admin')->get();
            $users =User::where('role', 'Manager')->get();
            $emails = [];
            foreach ($users as $user){
                array_push($emails, $user->email);
            }
            Mail::to($admin[0]->email)->cc($emails)->later($when, new taskUpdateNotification($task, $holdit));
            return response()->json(['task'=>$task]);
        }catch (\Exception $e){
            return $e->getMessage();
        }
    }

    function updateCompletionDate(Request $request){
        try{
            $task = task::where('id', $request->id)->first();
            $task->actual_completion_date = $request->date;
            $task->update();
            return response()->json(['task'=>'updated']);
        }catch (\Exception $e){
            return $e->getMessage();
        }
    }

    function endProject(Request $request){
        try{
            $totalCount = processCount::where('project_id', $request->id)->count();
            $projectcount = processCount::where('project_id', $request->id)->where('process_count', 8)->count();
            if($projectcount){
                $project = project::where('id', $request->id)->first();
                $project->completion_status = 1;
                $project->update();
                return response()->json(['success'=>'updated']);
            }else{
                return response()->json(['error'=>'tasks not completed']);
            }
        }catch(\Exception $e){
            return $e->getMessage();
        }
    }

    function editTask(Request $request){
        try{
            $task = task::where('id', $request->id)->first();
            if($task->task_name != $request->name){
                $task->task_name = $request->name;
            }
            if($task->expected_start_date != $request->sdate){
                $task->expected_start_date = $request->sdate;
            }
            if($task->expected_finish_date != $request->edate){
                $task->expected_finish_date = $request->edate;
            }
            if($task->description != $request->description){
                $task->description = $request->description;
            }
            if($task->amount_due != $request->amt){
                $task->amount_due = $request->amt;
            }
            if($task->client_email != $request->cemail){
                $task->client_email = $request->cemail;
            }
            if($task->client_name != $request->cname){
                $task->client_name = $request->cname;
            }
            if($task->project_type != $request->projecttype){
                $task->project_type = $request->projecttype;
            }
            if($task->supervisor != $request->supervisor){
                $task->supervisor = $request->supervisor;
            }
            if($task->vendor != $request->vendor){
                $task->vendor = $request->vendor;
            }
            $task->updated_by = Auth::id();
            $task->update();
            $when = Carbon::now()->addMinute(3);
            $admin = User::where('role', 'Admin')->get();
            $users =User::where('role', 'Manager')->get();
            $emails = [];
            foreach ($users as $user){
                array_push($emails, $user->email);
            }
            Mail::to($admin[0]->email)->cc($emails)->later($when, new editTaskNotification($task));
            return response()->json(['success'=>'updated']);
        }catch (\Exception $e){
            return response()->json(['error'=>'error']);
        }

    }

    function updateTaskStatus(Request $request){
        try{
            $holdit ="";
            $task = task::where('id', $request['taskId'])->first();

            if($request['stage'] == "quotation_status"){
                $task->quotation_status = 1;
            }elseif($request['stage'] == "client_approval"){
                $task->client_approval = 1;
            }elseif($request['stage'] == "head_office_informed"){
                $task->head_office_informed = 1;
            }elseif ($request['stage'] == "head_office_approval"){
                $task->head_office_approval = 1;
            }elseif ($request['stage'] == "execution_status"){
                $task->execution_status = 1;
            }elseif ($request['stage'] == "jcc_status"){
                $task->jcc_status = 1;
            }elseif ($request['stage'] == "invoice_status"){
                $task->invoice_status = 1;
            }elseif ($request['stage'] == "payment_status"){
                $task->payment_status = 1;
            }
            $holdit = $request['stage'];
            $task->update();
            $incrementProcessCount = processCount::where(['task_id'=>$task->id, 'project_id'=>$task->project_id])->first();
            if($incrementProcessCount){
                if($incrementProcessCount->process_count < 8){
                    //$incrementProcessCount->process_count = $incrementProcessCount->process_count + 1;
                    if($request['stage'] == "quotation_status"){
                        $incrementProcessCount->process_count = 1;
                    }elseif($request['stage'] == "client_approval"){
                        $incrementProcessCount->process_count = 2;
                    }
                    elseif($request['stage'] == "head_office_informed"){
                        $incrementProcessCount->process_count = 3;
                    }elseif ($request['stage'] == "head_office_approval"){
                        $incrementProcessCount->process_count = 4;
                    }elseif ($request['stage'] == "execution_status"){
                        $incrementProcessCount->process_count = 5;
                    }elseif ($request['stage'] == "jcc_status"){
                        $incrementProcessCount->process_count = 6;
                    }elseif ($request['stage'] == "invoice_status"){
                        $incrementProcessCount->process_count = 7;
                    }elseif ($request['stage'] == "payment_status"){
                        $incrementProcessCount->process_count = 8;
                    }
                    $incrementProcessCount->update();
                }
            }else{
                processCount::create([
                    'project_id' => $task->project_id,
                    'task_id' => $task->id,
                    'process_count' => 0+1,
                    'updated_by' => Auth::id()
                ]);
            }
//            return response()->json(['task'=>$task]);
            $users = User::all();
            $emails = [];
            foreach ($users as $user){
                array_push($emails, $user);
            }
            Mail::to($users)->later(Carbon::now('Africa/Lagos')->addMinute(3), new taskUpdateNotification($task, $holdit));
            return view('updateFromMailResponse')->with(['task'=>$task]);
        }catch (\Exception $e){
            //return $e->getMessage();
            abort(505);
        }
    }

    function generateInvoice($id){
        try{
            $task = task::where('id', $id)->first();
            $pdf = PDF::loadView('invoice', ['task'=>$task]);
            return $pdf->download('invoice.pdf');
        }catch(\Exception $e){
            //return $e->getMessage();
            abort(505);
        }
    }

    function unfinishedTasks(){
        try{
            $tasks = task::where('payment_status', 0)->get();
            return view('displayUnfinishedTask',['tasks'=>$tasks]);
        }catch (\Exception $e){
            abort(503);
        }
    }

    function unfinishedProjects(){
        try{
            $projects = project::where('completion_status', 0)->get();
            return view('displayUnfinishedProjects', ['projects'=>$projects]);
        }catch (\Exception $e){
            abort(503);
        }
    }

    function settings(){
        if(Auth::check()){
            $user = User::where('id', Auth::user()->id)->first();
            return view('settings')->with(['user'=>$user, 'page'=>'settings']);
        }else{
            \Session::put('errorMessage', 'You are not authorized');
            return redirect(route('logUserOut'));
        }
    }


    function  updateMe(Request $request){
        try{
            if($request != null){
                $user = User::where('id', Auth::id())->first();
                if($user){
                    $user->email = $request->email;
                    $user->firstname = $request->firstname;
                    $user->lastname = $request->lastname;
                    \Session::put('successfulMessage', 'You updated your info.');
                    return redirect()->back();
                }
            }
        }catch (\Exception $e){
            \Session::put('errorMessage', 'Error in connection, Try again');
            return redirect()->back();
        }

    }

    function changePassword(Request $request){
        try{
            if(Auth::user()){
                $user = User::findOrFail(Auth::id());
                if(Hash::check($request->oldpassword, $user->password))
                {
                    if($request->newpassword == $request->confirmnewpassword){
                        $user->password = bcrypt($request->newpassword);
                        $user->update();
                        $info = "Password Updated successfully";
                        \Session::put('successfulMessage', $info);
                        return redirect()->back();
                    }else{
                        $error = "Ensure your new password and confirm new password are the same.";
                        \Session::put('errorMessage', $error);
                        return redirect()->back();
                    }
                }else{
                    $error = "Your password does not match what we have in record";
                    \Session::put('errorMessage', $error);
                    return redirect()->back();
                }
            }
        }catch (\Exception $e){
            $error = "There was a an error in connection, Try again";
            \Session::put('errorMessage', $error);
            return redirect()->back();
        }
    }

    function editproject(Request $request){
        try{

            $project = project::where('id', $request->id)->first();
            $previousName = $project->name;
            $thenowClient = $project->client;
            $project->start_date = $request->startdate;
            $project->finish_date = $request->enddate;
            $project->name = $request->name;
            $project->client = $request->client;
            $project->budget = $request->budget;
            $project->save();
            if($thenowClient !== $project->client){
                $tasks = task::where('project_id', $project->id)->get();
                foreach ($tasks  as $task)
                    $task->client_name = $project->client;
                    $task->update();
            }

            $admin = User::where('role', 'Admin')->get();
            $users =User::where('role', 'Manager')->where('suspend', 0)->get();
            $emails = [];
            Mail::to($admin[0]->email)->send(new projectEdit($project, $admin[0], $previousName));
//            , $admin[0]->role
            if(sizeof($users) > 0){
                foreach($users as $user){
                    //array_push($emails, $user->email);
                    Mail::to($user->email)->send(new projectEdit($project, $user, $previousName));
//                , $user->role
                }
            }
            return "success";
        }catch (\Exception $e){
            return "error";
        }
    }

    function  addClient(Request $request){
        try{
            $client = client::create([
                'name' => $request->clientname,
                'client_email' => $request->clmail,
                'assigned_staff' => $request->smail,
            ]);
            if($client){
                return response()->json(['success'=>$client]);
            }else{
                return "error";
            }
        }catch (\Exception $e){
            return "error";
        }
    }

    function  addVendor(Request $request){
        try{
            $vendor = vendor::create([
                'name' => $request->vendorname,
                'location' => $request->location,
            ]);
            if($vendor){
                return response()->json(['success'=>$vendor]);
            }else{
                return "error";
            }
        }catch (\Exception $e){
            return "error";
        }
    }

    function  allClients(){
        $clients = client::all();
        return view('allClients', ['clients'=>$clients]);
    }

    function  allVendors(){
        $vendors = vendor::all();
        return view('allVendors', ['vendors'=>$vendors]);
    }

    function editClient(Request $request){
        try{
            $client = client::findOrFail($request->id);
            $client->name = $request->clientname;
            $client->assigned_staff = $request->smail;
            $client->client_email = $request->clmail;
            $client->save();
            return response()->json(['success'=>$client]);
        }catch (\Exception $e){
            return "error";
        }
    }

    function editVendor(Request $request){
        try{
            $vendor = vendor::findOrFail($request->id);
            $vendor->name = $request->vendorname;
            $vendor->location = $request->location;
            $vendor->save();
            return response()->json(['success'=>$vendor]);
        }catch (\Exception $e){
            return "error";
        }
    }

    function approveProject(Request $request){
        if(Auth::check()){
            if(Auth::id() == $request->user){
                $project = project::where('id', $request->projectId)->first();
                if($project){
                    if($project->approval_status == 0){
                        $project->approval_status = 1;
                        $project->approved_by = Auth::id();
                        $project->save();
                        $user = User::where('id', $project->created_by)->first();
                        Mail::to($user->email)->send(new ApprovedProject($project));
                        Session::put('successfulMessage', 'Project Approved');
                    }else{
                        Session::put('errorMessage', 'Project already approved by another authorized person');
                    }
                    return redirect(route('dashboard'));
//                return view('projectApproved');
                }
                Session::put('errorMessage', 'Project does not exist');
                return redirect(route('dashboard'));
            }else{
                Session::put('errorMessage', 'You are not authorized');
                return redirect(route('logUserOut'));
            }
        }else{
            if(Auth::loginUsingId($request->user)){
                $project = project::where('id', $request->projectId)->first();
                if($project){
                    if($project->approval_status == 0){
                        $project->approval_status = 1;
                        $project->approved_by = Auth::id();
                        $project->save();
                        $user = User::where('id', $project->created_by)->first();
                        Mail::to($user->email)->send(new ApprovedProject($project));
                        Session::put('successfulMessage', 'Project Approved');
                    }else{
                        Session::put('errorMessage', 'Project already approved by another authorized person');
                    }
//                return redirect(route('dashboard'));
                    return view('projectApproved');
                }else{
                    Session::put('errorMessage', 'Project does not exit any longer. It is most likely deleted');
                    return redirect(route('dashboard'));
                }

            }else{
                Session::put('errorMessage', 'You are not authorized');
                return redirect(route('logUserOut'));
            }
        }
    }


    function checkIfApproved(Request $request){
        try{
            $project = project::where('id', $request->id)->first();
            if($project){
                if($project->approval_status == 1){
                    return response()->json(['approved'=>$project]);
                }else{
                    return response()->json(['notApproved'=>$project]);
                }
            }else{
                return false;
            }
        }catch (\Exception $e){
            return $e->getMessage();
        }

    }

    function requestApproval($id){
        try{
            $project = project::where('id', $id)->first();
            $admin = User::where('role', 'Admin')->get();
            $users =User::where('role', 'Manager')->get();
            $emails = [];
            Mail::to($admin[0]->email)->send(new approvalRequest($project, $admin[0]));
            foreach ($users as $user){
                //array_push($emails, $user->email);
                Mail::to($user->email)->send(new approvalRequest($project, $user));
            }
            Session::put('successfulMessage', 'Approval Requested');
            return redirect()->back();
        }catch (\Exception $e){
            Session::put('errorMessage', 'Approval could not be Requested. Try again.');
            return redirect()->back();
        }

    }

    function approveTask(Request $request){
        $task = task::findOrFail($request->taskId);
        if($task){
            $task->approval_status = 1;
            $task->project->approval_status = 1;
            $task->save();
            Session::put('successfulMessage', 'Task successfully approved');
            $user = User::where('id', $task->created_by)->first();
            $accountant = User::where('role', 'Accountant')->first();
            if($accountant){
                Mail::to($user->email)->cc($accountant->email)->send(new taskApproved($task));
            }else{
                Mail::to($user->email)->send(new taskApproved($task));
            }

            return view('response', ['message'=>'Task successfully approved']);
        }
        Session::put('errorMessage', 'Task successfully approved');
        return view('response', ['message'=>'There was error approving the task, please try again.']);
    }

    function  downloadfile(Request $request){
//        return public_path('storage/projectDocuments/'.$request->idEnt1f1.'/'.$request->link);
//        return response()->download(storage_path('app/projectDocuments/'.$request->link));
        $exists = Storage::disk('local')->exists('public/projectDocuments/'.$request->idEnt1f1.'/'.$request->link);
//        return $exists;
        if($exists){
            return response()->download(public_path('storage/projectDocuments/'.$request->idEnt1f1.'/'.$request->link));
//            Storage::download(public_path('storage/projectDocuments/'.$request->idEnt1f1.'/'.$request->link));
        }else{
            Session::put('errorMessage', 'File does not exist');
            return redirect()->back();
        }

//        return response()->download(public_path('storage/projectDocuments/'.$request->link));
    }

    function deletedocfile(Request $request){
        try{
            $record = project::find($request->id);
            $exists = Storage::disk('local')->exists('public/projectDocuments/'.$request->id.'/'.$request->name);
            if($exists){
//                Storage::disk('local')->delete('public/projectDocuments/'.$request->id.'/'.$request->name);
                Storage::delete('public/projectDocuments/'.$request->id.'/'.$request->name);
                /*File::delete(storage_path('public/projectDocuments/'.$request->id.'/'.$request->name));
                File::delete(public_path('storage/projectDocuments/'.$request->id.'/'.$request->name));*/
                $record->document = null;
                $record->update();
                return response()->json(['success'=>'Success']);
            }
            else{
                $record->document = null;
                $record->update();
                return response()->json(['error'=>'File does not exist']);
            }
        }catch (\Exception $error){
            return response()->json(['error'=>'Server error']);
        }

    }

    function uploadDocumentImage(Request $request){
        if($request->hasFile('file')){
            $file = $request->file('file');
            $filevalue = $file->getClientOriginalName();
            Storage::deleteDirectory('public/projectDocuments/'.$request->id);
            Storage::putFileAs('public/projectDocuments/'.$request->id, $file, $file->getClientOriginalName());
            $project = project::find($request->id);
            $project->document = $filevalue;
            $project->update();
            return response()->json(['success'=>'success', 'id'=>$request->id, 'name'=>$filevalue]);
        }
    }

    function skipNextStep(Request $request){
        try{
            $holdit ="";
            $skipped = "";
            $task = task::where('id', $request['task_id'])->first();
            if($request['current_step'] == "nothing"){
                $task->quotation_status = 0;
                $task->client_approval = 1;
                $skipped = 'quotation_sent';
            }
            elseif($request['current_step'] == "quotation_sent"){
                $task->client_approval = 0;
                $task->head_office_informed = 1;
                $skipped = 'quotation_approved';
                //$holdit = 'head_office_informed';
            }elseif($request['current_step'] == "quotation_approved"){
                $task->head_office_informed = 0;
                $task->head_office_approval = 1;
                $skipped = 'head_office_informed';
                //$holdit = $request[1];
            }
            elseif($request['current_step'] == "head_office_informed"){
                $task->head_office_approval = 0;
                $task->execution_status = 1;
                $skipped = 'execution_status';
            }elseif ($request['current_step'] == "head_office_approval"){
                $task->execution_status = 0;
                $task->jcc_status = 1;
            }elseif ($request['current_step'] == "execution_status"){
                $task->jcc_status = 0;
                $task->invoice_status = 1;
                $skipped = 'jcc_status';
            }elseif ($request['current_step'] == "jcc_status"){
                $task->invoice_status = 0;
                $task->payment_status = 1;
                $skipped = 'invoice_status';
            }elseif ($request['current_step'] == "invoice_status"){
                //$task->invoice_status = 1;
            }elseif ($request['current_step'] == "payment_status"){
                //$task->payment_status = 1;
            }
            $holdit = $request['current_step'];
            $untracked = 0;
            if($skipped != ""){
                if($task->skipped == null){
                    $tempArray = [];
                    if(!in_array($skipped, $tempArray)){
                        array_push($tempArray, $skipped);
                    }
//                    $task->skipped = implode(',', $tempArray);
                    $task->skipped = $tempArray;
                    $untracked = count($tempArray);
                }else{
                    $tempArray = [];
                    $tempArray = $task->skipped;
                    if(!in_array($skipped, $tempArray)){
                        array_push($tempArray, $skipped);
                    }

//                    $task->skipped = implode(',', $tempArray);
                    $task->skipped = $tempArray;
                    $untracked = count($tempArray);
                }
            }
            $task->update();
            $incrementProcessCount = processCount::where(['task_id'=>$task->id, 'project_id'=>$task->project_id])->first();
            if($incrementProcessCount){
                if($incrementProcessCount->process_count < 8){
                    //$incrementProcessCount->process_count = $incrementProcessCount->process_count + 1;
                    if($skipped == "quotation_approved"){
                        $incrementProcessCount->process_count = 3;
                    }
                    elseif($skipped == "head_office_informed"){
                        $incrementProcessCount->process_count = 4;
                    }elseif ($skipped == "head_office_approval"){
                        $incrementProcessCount->process_count = 5;
                    }elseif ($skipped == "execution_status"){
                        $incrementProcessCount->process_count = 6;
                    }elseif ($skipped == "jcc_status"){
                        $incrementProcessCount->process_count = 7;
                    }elseif ($skipped == "invoice_status"){
                        $incrementProcessCount->process_count = 8;
                    }elseif ($skipped == "quotation_sent"){
                        $incrementProcessCount->process_count = 2;
                    }
                    $incrementProcessCount->update();
                }
            }else{
                processCount::create([
                    'project_id' => $task->project_id,
                    'task_id' => $task->id,
                    'process_count' => 0+1,
                    'updated_by' => Auth::id()
                ]);
            }
            return response()->json(['task'=>$task]);
        }catch (\Exception $exception){
            return $exception->getMessage();
        }

    }

    function complain(){
        return view('clientComplaint');
    }

    function submitComplaint(Request $request){
        try{
            $validator = Validator::make($request->all(), [
                'email'=>'required',
                'company'=> 'required',
                'message'=> 'required',
                'name'=>'required'
            ]);

            if($validator->fails()){
                return response()->json(['validationError', $validator->errors()]);
            }

            $complaint = complaint::create([
                'email' => $request->email,
                'company' => $request->company,
                'message' => $request->message,
                'phone' => $request->phone,
                'complainants_name' => $request->name,
                'department' => $request->department,
                'location' => $request->location,
                'title' => $request->title,
                'date' => $request->date,
                'time' => $request->time,
                'location' => $request->location,
                'detailed_complaint' => $request->message,
                'witness_name' => $request->witness_name,
                'witness_email' => $request->witness_email,
                'witness_phone' => $request->witness_phone,
            ]);

            $users = User::all();
            $emails = [];
            foreach ($users as $user){
                array_push($emails, $user);
            }
            Mail::to($users)->send(new complaintNotification($complaint));

            return response()->json(['success'=>'Your complaint was successfully sent']);
        }catch (\Exception $e){
            return response()->json(['error'=>$e->getMessage()]);
        }

    }

    function fetchComplaints(){
        $complaints = complaint::all();
        return view('allComplaints')->with(['complaints'=>$complaints]);
    }

    function complaintDetails($id){
        $complaint = complaint::findOrFail($id);
        return view('complaintDetails')->with(['complaint'=>$complaint]);
    }

    function toggleResolve(){
        $complaint = complaint::findOrFail(\request('id'));
        if($complaint){
            $complaint->resolution_status = 1;
            $complaint->update();
            Mail::to($complaint->email)->send(new complaintRessolutionUpdate($complaint, 'resolved'));
            return response()->json(['success'=>'Complaint marked resolved.']);
        }else{
            return response()->json(['error'=>'Complaint does not exist']);
        }
    }

    function unResolve(){
        $complaint = complaint::findOrFail(\request('id'));
        if($complaint){
            $complaint->resolution_status = 0;
            $complaint->update();
            Mail::to($complaint->email)->send(new complaintRessolutionUpdate($complaint, 'unresolved'));
            return response()->json(['success'=>'Complaint marked unresolved.']);
        }else{
            return response()->json(['error'=>'Complaint does not exist']);
        }
    }

    function satisfied(){
        $complaint = complaint::findOrFail(\request('id'));
        if($complaint){
            $complaint->client_satisfaction = 1;
            $complaint->update();
            //return response()->json(['success'=>'You are satisfied.']);
            return view('clientSatisfactionResponse')->with(['response'=>'satisfied']);;
        }else{
            //return response()->json(['error'=>'Complaint does not exist']);
            return view('clientSatisfactionResponse')->with(['response'=>'error']);;
        }
    }

    function unsatisfied(){
        $complaint = complaint::findOrFail(\request('id'));
        if($complaint){
            $complaint->client_satisfaction = 0;
            $complaint->update();
//            return response()->json(['success'=>'You are not satisfied.']);
            return view('clientSatisfactionResponse')->with(['response'=>'unsatisfied']);
        }else{
//            return response()->json(['error'=>'Complaint does not exist']);
            Session::put('errorMessage', 'Action Could not be performed');
            return view('clientSatisfactionResponse')->with(['response'=>'error']);;
        }
    }
}
