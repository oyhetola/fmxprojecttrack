<?php

namespace App\Http\Controllers;

use App\Mail\confrimationMail;
use App\Mail\recoverPassword;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class registerController extends Controller
{
    Public function renderRegister(){
        return view('register');
    }

    public $successStatus = 200;
  
    function register(Request $request) {
        try{

            $validator = Validator::make($request->all(), [
                'firstname' => 'required',
                'lastname' => 'required',
                'email' => 'required|email|unique:users',
                'password' => 'required',
                'Confirm_Password' => 'required|same:password',
            ])->validate();
            /*if ($validator->fails()) {
                return response()->json(['errors'=> $validator->errors()], 401);
            }*/

            /*$validateData = $request->validate([
                'firstname' => 'required',
                'lastname' => 'required',
                'email' => 'required|email|unique:users',
                'password' => 'required',
                'Confirm_Password' => 'required|same:password',
            ]);*/
            //return $validateData;

            $input= $request->all();
            $input['password'] = Hash::make($request->password);
            $input['user_role'] = 'candidate';
            $input['confirmationToken'] = Str::random(50);
            $user = User::create($input);
            //$success['token'] =  $user->createToken('TalentHub')->accessToken;
            if($user){
                //Mail::to($user->email)->send(new confrimationMail($user));
                return response()->json(['success'=>$user], $this->successStatus);
            }else{
                return response()->json(['error'=>"oops we cannot create this account, try again"]);
            }

        }catch (\Exception $e){
            return $e->getMessage();
        }
    }


    /*function confirmAccount($token){
        $user = User::where('confirmationToken', $token)->first();
        if(Auth::loginUsingId($user->id)){
            $user->confirmationToken = "";
            $user->confirmation_status = 1;
            $user->update();
            Session::put('successfulMessage', 'Your Account has been confirmed');
            return redirect()->intended('dashboard');
        }else{
            Session::put('errorMessage', 'Your Account cannot be confirmed at the moment.');
            return redirect(route('viewLogin'));
        }
    }*/

    function  renderPasswordRecoverPage(){
        return view('recoverpassword');
    }

    function recoverPass(Request $request){
        try{
            $user = User::where('email', $request->email)->first();
            if($user){
                $str = Str::random(50);
                $user->recoveryToken = $str;
                $user->save();
                $user['newString'] = $str;

                Mail::to($user->email)->send(new recoverPassword($user));
                Session::put('successfulMessage', 'A mail containing the next instructions has been sent to your email. Please check your inbox');
                return redirect()->back();
            }else{
                Session::put('errorMessage', 'Your email does not exist in our record');
                return redirect()->back();
            }
        }catch (\Exception $e){
            Session::put('errorMessage', 'Snap! Sth went wrong try again');
            return redirect()->back();
        }

    }

    function createNewPassword($id){
        try{
            return view('enterNewPassword', ['token'=>$id]);
        }catch (\Exception $e){
            abort('505');
        }

    }

    function changePassword(Request $request){
        try{
            $validator = Validator::make($request->all(), [
                'password' => 'required',
                'confirmNewPassword' => 'required|same:password',
            ]);

            if ($validator->fails()) {
                return redirect()->back()->withErrors($validator->errors());
            }
            $user = User::where('recoveryToken', $request->token)->first();
            if($user){
                $user->password = bcrypt($request->password);
                $user->save();
                Session::put('successfulMessage', 'You successfully changed your password you can now login');
                return view('passwordChangeSuccessful');
            }else{
                Session::put('errorMessage', 'We cannot validate you');
                return redirect()->back();
            }
        }catch (\Exception $e){
            $error = "There was a an error in connection, Try again";
            Session::put('errorMessage', $error);
            return redirect()->back();
        }
    }
}
