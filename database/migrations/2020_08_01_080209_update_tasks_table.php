<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateTasksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tasks', function (Blueprint $table){
            $table->string('project_type')->nullable();
            $table->boolean('approval_status')->default(0)->nullable();
            $table->string('vendor')->nullable();
            $table->string('supervisor')->nullable();
            $table->string('skipped')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tasks', function (Blueprint $table){
            $table->dropColumn('project_type');
            $table->dropColumn('approval_status');
            $table->dropColumn('vendor');
            $table->dropColumn('supervisor');
            $table->dropColumn('skipped');
        });
    }
}
