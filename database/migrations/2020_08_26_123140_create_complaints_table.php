<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateComplaintsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('complaints', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('company');
            $table->string('department');
            $table->string('email');
            $table->string('phone');
            $table->string('title')->nullable();
            $table->string('date')->nullable();
            $table->string('time')->nullable();
            $table->string('location');
            $table->longText('detailed_complaint');
            $table->string('witness_name')->nullable();
            $table->string('witness_email')->nullable();
            $table->string('witness_phone')->nullable();
            $table->string('complainants_name')->nullable();
            $table->string('resolution_status')->default(0);
            $table->string('client_satisfaction')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('complaints');
    }
}
