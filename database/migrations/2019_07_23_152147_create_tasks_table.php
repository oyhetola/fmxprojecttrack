<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTasksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tasks', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('project_id');
            $table->string('task_name');
            $table->string('description');
            $table->boolean('quotation_status')->default(false);
            $table->boolean('client_approval')->default(false);
            $table->boolean('head_office_informed')->default(false);
            $table->boolean('head_office_approval')->default(false);
            $table->string('approval_date')->nullable();
            $table->string('client_name')->nullable();
            $table->string('client_email')->nullable();
            $table->boolean('execution_status')->default(false);
            $table->boolean('invoice_status')->default(false);
            $table->boolean('payment_status')->default(false);
            $table->float('amount_due')->nullable();
            $table->float('amount_paid')->nullable();
            $table->boolean('jcc_status')->default(false);
            $table->string('expected_start_date')->nullable();
            $table->string('expected_finish_date')->nullable();
            $table->string('actual_completion_date')->nullable();
            $table->integer('created_by');
            $table->string('updated_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tasks');
    }
}
